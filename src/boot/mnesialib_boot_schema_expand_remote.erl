%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.02.2021
%%% @doc Expands schema to node (running on master)

-module(mnesialib_boot_schema_expand_remote).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([request_expand_schema/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("boot.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------------
%% Expands schema by request from other node
%% ---------------------------
request_expand_schema({?EXPANDSCHEMA_KEY, FromNode}) ->
    ?BLopts:set_group_leader(),
    ?OUT('$force', " ~s. Mnesia got request_expand_schema from ~p", [?RoleName,FromNode]),
    case ?BootUtils:check_schema() of
        false -> false;
        true ->
            case lists:keyfind(mnesia, 1, application:which_applications()) of
                false -> false;
                _ ->
                    case mnesia:system_info(db_nodes) of
                        [] -> false;
                        Nodes when is_list(Nodes) ->
                            % @todo check if FromNode is in configuration
                            expand_schema_to_node(FromNode)
                    end end end;
request_expand_schema(_) ->
    {error, "access denied to expand schema"}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% check cfg role
expand_schema_to_node(ToNode) ->
    CfgNodes = ?MU:mnesia_cfg_nodes(),
    case lists:member(ToNode, CfgNodes) of
        true ->
            expand_schema_to_node_1(ToNode, CfgNodes);
        false ->
            ?OUT('$force', " ~s. Mnesia expand_schema_to_node '~s' refused by cfg of '~s'", [?RoleName, ToNode, node()]),
            false
    end.

%% @private
%% check schema equals to cfg
expand_schema_to_node_1(ToNode, CfgNodes) ->
    DbNodes = mnesia:system_info(db_nodes),
    case lists:sort(DbNodes)==lists:sort(CfgNodes) of
        true ->
            do_expand_schema_to_node(ToNode);
        false ->
            expand_schema_to_node_2(ToNode, CfgNodes, DbNodes)
    end.

%% @private
%% check schema should be reduced
expand_schema_to_node_2(ToNode, CfgNodes, DbNodes) ->
    case DbNodes -- CfgNodes of
        [_|_]=ExcessNodes ->
            ?OUT('$force', " ~s. Mnesia expand_schema_to_node '~s' paused by '~s' (1). Excess nodes found: ~120tp.", [?RoleName, ToNode, node(), ExcessNodes]),
            retry_after;
        [] ->
            expand_schema_to_node_3(ToNode, CfgNodes, DbNodes)
    end.

%% @private
%% check schema should be enlarged (and not A1-A2, B1-B2)
expand_schema_to_node_3(ToNode, _CfgNodes, _DbNodes) ->
    %ExNodes = CfgNodes -- DbNodes,
    {M,F,A} = {?MU,check_running,[]},
    Res = ?BLmulticall:call_direct([ToNode], {M, F, A}, 1000),
    case lists:any(fun({_,true}) -> true; (_) -> false end, Res) of
        true ->
            do_expand_schema_to_node(ToNode);
        false ->
            ?OUT('$force', " ~s. Mnesia expand_schema_to_node '~s' paused by '~s' (2). App mnesia not running there.", [?RoleName, ToNode, node()]),
            retry_after
    end.

%% @private
%% expand schema to node
do_expand_schema_to_node(ToNode) ->
    ?OUT('$force', " ~s. Mnesia expand_schema_to_node '~s'", [?RoleName, ToNode]),
    R1 = mnesia:change_config(extra_db_nodes, [ToNode]),
    ?OUT('$force', " ~s. .. mnesia:change_config -> ~120p", [?RoleName, R1]),
    R2 = mnesia:change_table_copy_type(schema, ToNode, disc_copies),
    ?OUT('$force', " ~s. .. mnesia:change_table_copy_type -> ~120p", [?RoleName, R2]),
    true.