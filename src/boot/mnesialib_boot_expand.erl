%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.02.2021
%%% @doc Boot routines to synchronize distributed schema
%%% -------------------------------------------------------------------

-module(mnesialib_boot_expand).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([boot/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("boot.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------
%% Boots mnesia
%% ---------------------------
-spec boot(State::term()) ->
            {ok, State::term()} |
            {retry_simple, Timeout::non_neg_integer(), Reason::string(), State::term()} |
            {retry_after, Timeout::non_neg_integer(), Reason::string(), State::term()}.
%% ---------------------------
boot(State) ->
    case application:get_env(mnesia,skip_boot) of
        {ok,true} -> {retry_simple, ?TIMEOUT_SIMPLE, "mnesia start skipped", State};
        _ -> boot_1(State)
    end.

%% @private
boot_1(State) ->
    ?OUT('$force', " ~s. ~p booting mnesia", [?RoleName, self()]),
    Status = mark_check_failure(),
    ?OUT('$force', " ~s. ~p   check status: ~120tp", [?RoleName, self(), Status]),
    mark_start(),
    case catch ?BootUtils:application_start(mnesia) of
        ok ->
            mark_clear(),
            after_boot(State);
        {error,{already_started,mnesia}} ->
            mark_clear(),
            after_boot(State);
        _T ->
            ?OUT('$force', " ~s. mnesia start error: ~120p", [?RoleName, _T]),
            ?MU:clear_directory(),
            {retry_after, ?TIMEOUT_ERRORAPP, "mnesia start error", State}
    end.

%% ---------------------
mark_start() ->
    ?OUT('$force', " ~s. ~p   mark start", [?RoleName, self()]),
    Filename = mark_filename(),
    file:write_file(Filename, <<"\n", (?BU:to_binary(?BU:timestamp()))/binary>>, [append]).
mark_clear() ->
    ?OUT('$force', " ~s. ~p   mark clear", [?RoleName, self()]),
    file:delete(mark_filename()).
mark_check_failure() ->
    Filename = mark_filename(),
    case file:read_file(Filename) of
        {ok,Data} ->
            Lines = binary:split(Data,<<"\n">>,[global]),
            NowTS = ?BU:timestamp(),
            Interval = 180,
            Timestamps = lists:filtermap(fun(Line) ->
                                                case ?BU:to_int(Line,0) of
                                                    TS when NowTS - TS > Interval*1000 -> false;
                                                    TS -> {true, TS}
                                                end
                                         end, Lines),
            case lists:reverse(Timestamps) of
                [] -> mark_clear();
                X when length(X) < 3 -> ok;
                Tss ->
                    ?OUT('$force', " ~s. mnesia found previous failures at last ~p sec: ~120p", [?RoleName, Interval, [NowTS-Ts || Ts <- Tss]]),
                    ?MU:clear_directory()
            end;
        _ -> ok
    end.

mark_filename() ->
    {ok,CWD} = file:get_cwd(),
    filename:join(CWD,"mnesia_marks.data").

%% ---------------------


%% @private
after_boot(State) ->
    ping_nodes(State),
    case ?BootUtils:check_schema() of
        true ->
            ?BootRebuildSchema:rebuild_schema(State);
        false ->
            ?BootUtils:application_stop(mnesia),
            ?BootMakeSchema:make_schema(State)
    end.

%% @private
%% ping schema nodes
ping_nodes(_State) ->
    SchemaNodes = ?MU:mnesia_cfg_nodes(),
    ?OUT('$force', " ~s. Mnesia. Ping ~120p", [?RoleName, SchemaNodes]),
    [net_adm:ping(Node) || Node <- SchemaNodes].


