%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.03.2017
%%% @doc #130 Implements strategies for mnesia unsplit

-module(mnesialib_unsplit).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

-export([push/1, push/2]).
-export([default_method/2]).
-export([all_both/3]).
-export([local/2,
         remote/2,
         full_merge/2,
         asc/2,
         select/2]).
-export([remote_handle_query/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-record(x, {
  state='init',
  args=[],
  keys_total=0,
  keys_done=0,
  objs_portion=[],
  lkeys=[],
  rkeys=[]
}).

%% ====================================================================
%% Public functions
%% ====================================================================

push(ToNode) ->
    % mnesia_controller:connect_nodes([ToNode]). % doesn't work properly
    case whereis(unsplit_server) of
        Pid when is_pid(Pid) -> push(Pid,ToNode);
        _ -> {error,process_not_found}
    end.

push(Pid,ToNode) ->
    % mnesia_controller:connect_nodes([ToNode]). % doesn't work properly
    Pid ! {mnesia_system_event, {inconsistent_database, mnesialib_repair_found_partitioned_network, ToNode}}.

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------
%% default method of unsplit
%% ------------------------
default_method(init,[_Table,_RecFields|_]) -> stop.

%% ------------------------
%% Strategy of search
%% ------------------------
all_both(Tab, Remote, #x{state=init}=MSt) ->
    HL = get_hash(local, Tab),
    HR = get_hash(Remote, Tab),
    if HL==HR ->
           ?OUT('$info', "Mnesia unsplit '~s'. No difference found", [Tab]),
           {ok,done,MSt};
       true ->
           ?OUT('$warning', "Mnesia unsplit '~s'. Found difference. Sync start.", [Tab]),
           KeysL = get_keys(local, Tab),
           KeysR = get_keys(Remote,Tab),
           PKeysL = ?BU:portions(100,KeysL),
           PKeysR = ?BU:portions(100,KeysR1=KeysR--KeysL),
           %?LOG('$trace', "KeysL: ~120p~n", [PKeysL]),
           %?LOG('$trace', "KeysR: ~120p~n", [PKeysR]),
           MSt1 = MSt#x{state=work,
                        keys_total=length(KeysL)+length(KeysR1),
                        lkeys=PKeysL,
                        rkeys=PKeysR},
           ?OUT('$warning', "Mnesia unsplit '~s'. Portion count: {L:~p, R:~p}", [Tab, length(PKeysL), length(PKeysR)]),
           {ok, start, MSt1}
    end;
all_both(_, _, #x{objs_portion=[{A,B}|P], keys_done=CntD}=MSt) ->
    {ok, [{A, B}], MSt#x{keys_done=CntD+1, objs_portion=P}};
all_both(Tab, Remote, #x{lkeys=[Ks|L]}=MSt) ->
    ?LOG('$warning', "Mnesia unsplit '~s'. Local portion: ~120p", [Ks]),
    AHs = get_hashes_for(local, Tab, Ks),
    BHs = get_hashes_for(Remote, Tab, Ks),
    case lists:filtermap(fun({_,{X,X}}) -> false; ({K,_}) -> {true, K} end, lists:zip(Ks,lists:zip(AHs,BHs))) of
        [] -> all_both(Tab, Remote, MSt#x{lkeys=L});
        Ks1 ->
            ?LOG('$info', "Mnesia unsplit '~s'. Found changes in ~120p", [Ks1]),
            As = get_objs(local, Tab, Ks1),
            Bs = get_objs(Remote, Tab, Ks1),
            all_both(Tab, Remote, MSt#x{objs_portion=lists:zip(As,Bs), lkeys=L})
    end;
all_both(Tab, Remote, #x{rkeys=[Ks|R]}=MSt) ->
    ?LOG('$warning', "Mnesia unsplit '~s'. Remote portion: ~120p", [Ks]),
    Bs = get_objs(Remote, Tab, Ks),
    As = lists:foldl(fun(_,Acc) -> [[]|Acc] end, [], Bs),
    all_both(Tab, Remote, MSt#x{objs_portion=lists:zip(As,Bs), rkeys=R});
all_both(Tab, _, #x{objs_portion=[],lkeys=[],rkeys=[],keys_total=CntT,keys_done=CntD}=MSt) ->
    ?LOG('$warning', "Mnesia unsplit '~s'. Updated ~120p records of ~120p", [Tab, CntD, CntT]),
    {ok, done, MSt}.

%% ------------------------
%% Strategy of choosing A (meaningful if table is not a set).
%% Applies write all found items to both islands.
%% ------------------------
local(init, Args) -> {ok, [], {?MODULE,all_both}, #x{state=init,args=Args}};
local(start, S) -> {ok, [], S};
local(done, _) -> stop;
local(Objs, S) ->
    AElts = lists:concat([A || {A,_B} <- Objs]),
    {ok, [{write, AElts}], S}.

%% ------------------------
%% Strategy of choosing A (meaningful if table is not a set).
%% Applies write all found items to both islands.
%% ------------------------
remote(init, Args) -> {ok, [], {?MODULE,all_both}, #x{state=init,args=Args}};
remote(start, S) -> {ok, [], S};
remote(done, _) -> stop;
remote(Objs, S) ->
    BElts = lists:concat([B || {_A,B} <- Objs]),
    {ok, [{write, BElts}], S}.

%% ------------------------
%% Strategy of merge (meaningful if table is not a set or if automatic full-tuple asc technology should be used).
%% Applies write all found items (not fully equal) to both islands.
%% ------------------------
full_merge(init, Args) -> {ok, [], {?MODULE,all_both}, #x{state=init,args=Args}};
full_merge(start, S) -> {ok, [], S};
full_merge(done, _) -> stop;
full_merge(Objs, S) ->
    Merged = lists:usort(lists:concat([A ++ B || {A,B} <- Objs])),
    {ok, [{write, Merged}], S}.

%% ------------------------
%% ascending by field and take max
%% Field index should be passed in parameter
%% ------------------------
asc(init, Args) -> {ok, [], {?MODULE,all_both}, #x{state=init,args=Args}};
asc(start, S) -> {ok, [], S};
asc(done, _) -> stop;
asc(Objs, #x{args=[_Tab,_Flds,FldNum]}=S) ->
    Merged = lists:concat([A ++ B || {A,B} <- Objs]),
    Elt = lists:foldl(fun(Tuple,undefined) -> Tuple;
                         (Tuple,Acc) ->
                              if element(FldNum,Tuple) > element(FldNum,Acc) -> Tuple;
                                 true -> Acc
                              end end, undefined, Merged),
    {ok, [{write, [Elt]}], S}.

%% ------------------------
%% select function
%% Function/2 or {M,F,A} should be passed in parameter.
%%   MFA's function is called of (ItemA, ItemB, ...A...)
%% ------------------------
select(init, [_Tab,_Flds,SelFun]=Args) when is_function(SelFun,2) ->
    {ok, [], {?MODULE,all_both}, #x{state=init,args=Args}};
select(init, [_Tab,_Flds,{M,F,EA}]=Args) when is_atom(M), is_atom(F), is_list(EA) ->
    case ?BU:function_exported(M,F,2 + length(EA)) of
        true -> {ok, [], {?MODULE,all_both}, #x{state=init,args=?BU:replace_nth(3,Args,fun(A,B) -> erlang:apply(M,F,[A,B|EA]) end)}};
        false ->
            % full_merge could be used
            stop
    end;
select(start, S) ->
    {ok, [], S};
select(done, _) ->
    stop;
select(Objs, #x{args=[_Tab,_Flds,SelFun]}=S) ->
    Merged = lists:concat([A ++ B || {A,B} <- Objs]),
    Elt = lists:foldl(fun(Tuple,undefined) -> Tuple;
                         (Tuple,Acc) -> SelFun(Tuple,Acc)
                      end, undefined, Merged),
    {ok, [{write, [Elt]}], S}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----
get_hash(local, Tab) ->
    L = mnesia:ets(fun() -> mnesia:foldl(fun(Obj,Acc) -> [Obj|Acc] end, [], Tab) end),
    erlang:phash2(lists:sort(L));
get_hash(Remote, Tab) ->
    ask_remote(Remote, {get_hash, Tab}).

%%% -----
get_keys(local, Tab) ->
    mnesia:dirty_all_keys(Tab);
get_keys(Remote, Tab) ->
    ask_remote(Remote, {get_keys, Tab}).

%%% -----
get_hashes_for(local, Tab, Keys) ->
    lists:reverse(lists:foldl(fun(Key,Acc) -> [erlang:phash2(mnesia:dirty_read({Tab, Key}))|Acc] end, [], Keys));
get_hashes_for(Remote, Tab, Keys) ->
    ask_remote(Remote, {get_hashes_for, Tab, Keys}).

%%% -----
get_obj(local, Tab, Key) ->
    mnesia:dirty_read({Tab,Key});
get_obj(Remote, Tab, Key) ->
    ask_remote(Remote, {get_obj, Tab, Key}).

%%% -----
get_objs(local, Tab, Keys) ->
    lists:reverse(lists:foldl(fun(Key,Acc) -> [mnesia:dirty_read({Tab, Key})|Acc] end, [], Keys));
get_objs(Remote, Tab, Keys) ->
    ask_remote(Remote, {get_objs, Tab, Keys}).

%%% -----
ask_remote(Remote, Q) ->
    ?BLrpc:call(Remote, ?MODULE, remote_handle_query, [Q]).

%% @private
remote_handle_query(Q) ->
    case Q of
        {get_hash, Tab} ->
            get_hash(local, Tab);
        {get_keys, Tab} ->
            get_keys(local, Tab);
        {get_hashes_for, Tab, Keys} ->
            get_hashes_for(local, Tab,Keys);
        {get_obj, Tab, Key} ->
            get_obj(local, Tab, Key);
        {get_objs, Tab, Keys} ->
            get_objs(local, Tab, Keys)
    end.
