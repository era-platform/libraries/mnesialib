%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.02.2021
%%% @doc Create table routines

-module(mnesialib_table_create).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([create_table/4,
         ping/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------
%% Creates table on all replicas in schema
%% ----------------------------------------
create_table(TabName, RecName, RecFields, Opts) ->
    {AppName,_} = ?TableProps:extract_app_owner(Opts),
    case catch do_create_table(TabName, RecName, RecFields, Opts) of
        {atomic, ok} ->
            ?OUT('$info', "  ~ts: mnesia table '~ts' created", [AppName, TabName]),
            ok;
        {aborted, {already_exists,_}} -> ok;
        {aborted, {not_active,_,Node}} ->
            {retry_after, ?TIMEOUT_WaitReplica, ?BU:str("Cannot create table '~ts': mnesia is down on node '~ts'", [TabName,Node])};
        {aborted, Reason} ->
            {retry_after, ?TIMEOUT_WaitReplica, ?BU:str("Cannot create table '~ts': ~1000tp", [TabName,Reason])};
        {'EXIT',_} ->
            {retry_after, ?TIMEOUT_Retry, "Exception caught"}
    end.

%% --------------------------------
ping() -> pong.

%% ====================================================================
%% Internal functions
%% ====================================================================

do_create_table(TabName, RecName, RecFields, Opts) ->
    CfgNodes = ?MU:mnesia_cfg_nodes(),
    Nodes = lists:filtermap(fun({Node,pong}) -> {true,Node}; (_) -> false end, ?BLmulticall:call_direct(CfgNodes,{?MODULE,ping,[]},2000)),
    TableType = maps:get(type, Opts, set),
    StorageProps = maps:get(storage_properties,Opts,[{ets, [compressed]}, {dets, [{auto_save, 5000}]}]),
    {ok,UserProps} = ?TableProps:build_table_properties(TabName,RecName,RecFields,Opts),
    ZipFields = lists:zip(lists:seq(2,length(RecFields)+1),RecFields),
    IndexFieldIdxs = lists:map(fun(Fld) when is_atom(Fld) -> element(1,lists:keyfind(Fld,2,ZipFields));
                                  (Fld) when is_integer(Fld) -> Fld
                               end, maps:get(index_fields,Opts,[])),
    Mode = case maps:get(mode, Opts, ?CFG:default_mode()) of
               undefined -> ?CFG:default_mode();
               default -> ?CFG:default_mode();
               _Mode -> _Mode
           end,
    CreateOptsion0 = case Mode of
                         disc ->
                             [{disc_copies, Nodes},
                              {ram_copies, []}];
                         ram ->
                             [{ram_copies, Nodes},
                              {disc_copies, []}];
                         disc_only ->
                             [{disc_only_copies, Nodes},
                              {disc_copies, []},
                              {ram_copies, []}];
                         custom ->
                             maps:get(mode_options, Opts, [{disc_copies, Nodes}]) % TODO: check validity
                     end,
    CreateOptions = [{type, TableType},
                     {record_name, RecName},
                     {attributes, RecFields},
                     {index, IndexFieldIdxs},
                     {storage_properties, StorageProps},
                     {user_properties, UserProps}
                     | CreateOptsion0],
    case mnesia:create_table(TabName, CreateOptions) of
        {atomic, ok}=Reply ->
%%            % to avoid automatic table copying from less relevant table copies.
%%            % to ensure fast loading on start of application.
%%            % to have time to unsplit.
%%            % it is stored locally (not distributed property)
%%            mnesia:set_master_nodes(TabName, [node()]),
%%            lists:foreach(fun(Node) ->
%%                              catch ?BLrpc:call(Node, mnesia, set_master_nodes, [TabName, [Node]], 5000)
%%                          end, Nodes--[node()]),
            Reply;
        Reply -> Reply
    end.