%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.02.2021
%%% @doc Reduce schema (after node is excluded from r's config it should be deleted from mnesia schema)
%%%      Concat schema (nodes combined to one group from two separated groups, folders not removed)
%%%      Checks schema locally, if it corresponds to config.

-module(mnesialib_repair_schema).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    check_schema/1,
    check_schema_noapps/1, check_schema_noapps/2
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("repair.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

check_schema(State) ->
    case mnesia:system_info(running_db_nodes) of
        [] -> {ok,State};
        _ ->
            case ?MU:check_running() of
                false -> {ok,State};
                true -> check_schema_1(State)
            end end.

check_schema_noapps(State) -> check_schema_noapps(undefined,State).
check_schema_noapps([], State) -> {ok,State};
check_schema_noapps(undefined, State) ->
    case mnesia:system_info(running_db_nodes) of
        [] -> {ok,State};
        _ -> check_schema_1(State)
    end;
check_schema_noapps(_, State) -> check_schema_1(State).

%% ====================================================================
%% Internal functions
%% ====================================================================


%% @private
%% take nodes lists
check_schema_1(State) ->
    DbNodes = ?MU:mnesia_schema_nodes(), % now in mnesia (nodes)
    CfgNodes = ?MU:mnesia_cfg_nodes(), % should be in mnesia
    AllNodes = ?CFG:all_nodes(), % all configuration nodes
    check_schema_2({DbNodes,CfgNodes,AllNodes},State).

%% check reduce (nodes exist in schema, absent in cfg)
check_schema_2({DbNodes,CfgNodes,AllNodes},State) ->
    case DbNodes -- CfgNodes of
        [_|_]=SchemaExcess ->
            % DbNodes larger than schema.
            case lists:member(node(),SchemaExcess) of
                true ->
                    % CASE (B)
                    ?OUT('$error', "MNESIA. Found current node not in schema. Clear & restart."),
                    ?MU:clear_directory(),
                    ?BU:stop_node(0),
                    {stop,State};
                false ->
                    % CASE (C)
                    case ?MU:reduce_schema_to_config() of
                        ok ->
                            DbNodes1 = ?MU:mnesia_schema_nodes(), % now in mnesia (nodes)
                            check_schema_3({DbNodes1,CfgNodes,AllNodes},State);
                        error ->
                            % CASE (C1) error reducing schema. Probably excluded node is still running
                            case State#state.schema_err_count of
                                ErrC when ErrC < 4 ->
                                    % CASE (C2) Count errors
                                    {skip,State#state{schema_err_count=ErrC+1}};
                                _ ->
                                    % CASE (C3) Goto boot mnesia reduce schema
                                    ?OUT('$error', "MNESIA. Found excess schema nodes: ~120p, Restarting node to rebuild schema.", [SchemaExcess]),
                                    ?BU:stop_node(3000),
                                    {stop,State}
                            end;
                        retry ->
                            {ok,State}
                    end
            end;
        [] ->
            check_schema_3({DbNodes,CfgNodes,AllNodes},State)
    end.

%% check node addr mismatch between schema and cfg
%% it's significant only if AllNodes and CfgNodes are different
check_schema_3({DbNodes,CfgNodes,AllNodes},State) ->
    case DbNodes -- AllNodes of
        [_|_] ->
            % DbNodes from schema fullname mismatch (same names on other addrs)
            ?OUT('$error', "MNESIA. Found current schema is node name@addr conflict (db_nodes vs config_all_nodes). Clear & restart."),
            ?MU:clear_directory(),
            ?BU:stop_node(0),
            {stop,State};
        [] ->
            % DbNodes correspond real cfg nodes by fullname
            check_schema_4({DbNodes,CfgNodes,AllNodes},State)
    end.

%% check extra nodes, absent in schema, existing in cfg
check_schema_4({DbNodes,CfgNodes,_AllNodes},State) ->
    case CfgNodes -- DbNodes of
        [] ->
            % Schema equal to cfg
            {ok,State};
        [_|_]=ExNodes ->
            % Schema differ to cfg, check if absent nodes are running mnesia
            {M,F,A} = {?MU,check_running,[]},
            Res = ?BLmulticall:call_direct(ExNodes, {M,F,A}, 1000),
            RunningExNodes = [N || {N,R} <- Res, R==true],
            case RunningExNodes of
                [] ->
                    % CASE (D). ExNodes not available or not running mnesia.
                    %  Skip. They should request schema themselves.
                    {ok,State};
                [_|_] ->
                    % CASE (E). ExNodes, absent in schema, are running mnesia. Schema sync error
                    [N || {N,R} <- Res, R==true],
                    check_schema_5({DbNodes,CfgNodes,RunningExNodes},State)
            end end.

%% check if configured schema is the same on all nodes
check_schema_5({DbNodes,CfgNodes,RunningExNodes},State) ->
    {M,F,A} = {?MU,mnesia_cfg_nodes,[]},
    Res = ?BLmulticall:call_direct(RunningExNodes, {M,F,A}, 1000),
    {_ErrR,OkR} = lists:partition(fun({_,undefined}) -> true; (_) -> false end, Res),
    {SameNodes,DiffNodes} = lists:partition(fun({_,R}) when R==CfgNodes -> true; (_) -> false end, OkR),
    % variants of next behaviour:
    %   skip iteration
    %   merge schemes
    %   clear local data and restart
    %   skip scheme update
    case {SameNodes,DiffNodes} of
        {[],_} when length(DiffNodes)<length(OkR) ->
            % skip iteration because connected stated changed
            {skip,State};
        {[],_} ->
            % skip merging schemes until the same configuration is found
            {ok,State};
        {_,[]} ->
            merge_schemes({DbNodes,CfgNodes,SameNodes},State);
        {_,_} ->
            merge_schemes({DbNodes,CfgNodes,SameNodes},State)
    end.

%% merge schemes of current node and some other nodes.
%% TODO: Update when you found out how to merge schemes of non-empty nodes.
%%       Probably it could be done by renaming of tables to local names, merging schemes and build new tables by unsplitted data from both nodes.
%%         But it is long operation.
merge_schemes({DbNodes,_CfgNodes,_SameCfgNodes},State) ->
    % now it's simply clearing data directory according to priority in CfgNodes
    NowTS = ?BU:timestamp(),
    SyncNodes = ?CFG:nodes_sync_sequence(),
    SkipIterations = ?BU:position(node(),SyncNodes),
    WaitTimeout = SkipIterations * 10000,
    Key = {?MODULE,?FUNCTION_NAME,clear_data_expire_ts,_SameCfgNodes},
    case ?BLstore:find_t(Key) of
        false when WaitTimeout>0 ->
            ?BLstore:store_t(Key,NowTS+WaitTimeout,WaitTimeout+30000),
            {ok,State};
        {_,TS} when TS >= NowTS -> {skip,State};
        _ ->
            ?BLstore:delete_t(Key),
            clear_data(DbNodes,State)
    end.

%% Clear local mnesia data and restart
clear_data(DbNodes,State) ->
    ?OUT('$error', "MNESIA. Found current schema configuration mismatch. ~n\tschema=~1000tp, ~n\t", [DbNodes]),
    ?MU:clear_directory(),
    ?BU:stop_node(0),
    {stop,State}.