%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.02.2021
%%% @doc

-module(mnesialib_table_properties).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([is_app_version_changed/4,
         get_app_owner/1,
         get_record_name/1,
         extract_app_owner/1,
         build_table_properties/4,
         update_table_properties/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(AppOwnerKey, 'app_owner').
-define(UnsplitKey, 'unsplit_method').

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------------------------
%% return true if app_owner|app_version property was changed
%% ---------------------------------------
is_app_version_changed(TabName, _RecName, _RecFields, Opts) ->
    New = extract_app_owner(Opts),
    case get_app_owner(TabName) of
        New -> false;
        _Old -> true
    end.

%% ---------------------------------------
%% return current table app_owner_property value: {AppName,AppVersion}
%% ---------------------------------------
get_app_owner(TabName) ->
    case catch mnesia:read_table_property(TabName, ?AppOwnerKey) of
        {'EXIT',_} -> undefined;
        {_, {_AppName,_AppVersion}=Prop} -> Prop
    end.

%% ---------------------------------------
%% return current table record name
%% ---------------------------------------
get_record_name(TabName) ->
    mnesia:table_info(TabName, record_name).

%% ---------------------------------------
%% return current table app_owner_property value: {AppName,AppVersion}
%% ---------------------------------------
extract_app_owner(Opts) ->
    AppName = maps:get(app_name, Opts, undefined),
    AppVersion = maps:get(app_version, Opts, undefined),
    {AppName,AppVersion}.

%% ---------------------------------------
%% Return new user properties for table from opts.
%% ---------------------------------------
build_table_properties(TabName, RecName, RecFields, Opts) ->
    Props0 = [app_owner_property(TabName, RecName, RecFields, Opts)
             | internal_properties(TabName, RecName, RecFields, Opts)],
    Props1 = case unsplit_property(TabName, RecName, RecFields, Opts) of
                 false -> Props0;
                 UnsplitProperty -> [UnsplitProperty | Props0]
             end,
    Props2 = case custom_properties(TabName, RecName, RecFields, Opts) of
                 false -> Props1;
                 CP -> CP ++ Props1
             end,
    {ok,Props2}.

%% ---------------------------------------
%% Update table by new user properties from opts
%% ---------------------------------------
update_table_properties(TabName, RecName, RecFields, Opts) ->
    {ok,Props} = ?MODULE:build_table_properties(TabName, RecName, RecFields, Opts),
    do_update_table_properties({TabName, Props}).

%% ====================================================================
%% Internal functions
%% ====================================================================


%% @private
%% for application update when delete
app_owner_property(_TabName,_RecName,_RecFields,Opts) ->
    {AppName,AppVersion} = extract_app_owner(Opts),
    {?AppOwnerKey, {AppName,AppVersion}}.

%% @private
%% TODO: hashcode of properties, etc.
internal_properties(_TabName,_RecName,_RecFields,_Opts) ->
    [].

%% @private
unsplit_property(_TabName,_RecName,RecFields,Opts) ->
    Method = maps:get(unsplit_method, Opts, undefined),
    AscFieldIndex = maps:get(unsplit_asc_field_index, Opts, undefined),
    SelectMFA = maps:get(unsplit_select_mfa, Opts, undefined),
    CustomMFA = maps:get(unsplit_custom_mfa, Opts, undefined),
    case {Method,AscFieldIndex,SelectMFA,CustomMFA} of
        {full_merge,_,_,_} ->
            {?UnsplitKey, {?Unsplit, full_merge, []}};
        %
        {asc,I,_,_} when is_integer(I), I>0, I<length(RecFields) ->
            {?UnsplitKey, {?Unsplit, asc, [I]}};
        {asc,_,_,_} ->
            throw({error,"Expected unsplit_asc_field_index for usplit_method 'asc'"});
        %
        {_,_,{M,F,EA},_} when (Method==select orelse Method==undefined) andalso is_atom(M) andalso is_atom(F) andalso is_list(EA) ->
            case ?BU:function_exported(M,F,2+length(EA)) of
                true -> {?UnsplitKey, {?Unsplit, select, [{M,F,EA}]}};
                false -> throw({error,"Invalid unsplit_mfa: function not exported"})
            end;
        {select,_,_,_} ->
            throw({error,"Expected unsplit_select_mfa for usplit_method 'select'"});
        %
        {custom,_,_,{M,F,A}} when is_atom(M) andalso is_atom(F) andalso is_list(A) ->
            case ?BU:function_exported(M,F,2+length(A)) of
                true ->  {?UnsplitKey, {M, F, A}};
                false -> throw({error,"Invalid unsplit_custom_mfa: function not exported"})
            end;
        {custom,_,_,_} ->
            throw({error,"Expected unsplit_custom_mfa for usplit_method 'custom'"});
        %
        {undefined,undefined,undefined,undefined} ->
            false;
        %
        _ ->
            throw({error,"Invalid unsplit settings"})
    end.

%% @private
custom_properties(_TabName,_RecName,_RecFields,Opts) ->
    case maps:get(custom_properties,Opts,[]) of
        [_|_]=CP -> CP;
        _ -> false
    end.

%% ----------------------------------------

%% (table should exist)
do_update_table_properties({Tab,_}=T) when is_atom(Tab) ->
    case catch mnesia:table_info(Tab,record_name) of
        A when is_atom(A) -> do_update_table_properties_1(T);
        _ -> ok
    end.

%% @private (fold by options, matches?)
do_update_table_properties_1({Tab,Props}) when is_atom(Tab) ->
    lists:foldl(fun({Key,_}=Prop, ok) ->
                    case catch mnesia:read_table_property(Tab, Key) of
                        Prop -> ok;
                        _ -> write_table_property(Tab, Prop)
                    end;
                    (_, Acc) -> Acc
                end, ok, Props).

%%%% @private (master node?)
%%write_table_property(Tab,Prop) ->
%%    {CurrentNode, [MasterNode|_]} = {node(), ?MU:mnesia_cfg_nodes()},
%%    case MasterNode==CurrentNode of
%%        false -> {retry_after, ?TIMEOUT_WaitMaster, ?BU:str("Wait for schema's master node to update table '~s'", [Tab])};
%%        true -> write_table_property_1(Tab,Prop)
%%    end.

%% @private (update props)
write_table_property(Tab,Prop) ->
    case mnesia:write_table_property(Tab,Prop) of
        {atomic, ok} -> ok;
        {aborted, {no_exists,_}} -> ok;
        {aborted, {not_active,_,_,Nodes}} ->
            {retry_after, ?TIMEOUT_WaitReplica, ?BU:str("Cannot update table '~s': mnesia is down on nodes ~1000p", [Tab,Nodes])};
        {aborted, Reason} ->
            {retry_after, ?TIMEOUT_WaitReplica, ?BU:str("Cannot update table '~s': ~1000p", [Tab,Reason])}
    end.