%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.02.2021
%%% @doc Boot gen_server to synchronize distributed schema.
%%%    Could remove local data, restart node, expand schema.
%%%    At the end change status and call callback function from opts
%%% -------------------------------------------------------------------

-module(mnesialib_boot_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([restart_sync/0,
         get_status/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("boot.hrl").

-define(TimeoutRetry, 5000).
-define(RetryMsg(Ref), {timer_retry,Ref}).

%% ====================================================================
%% Public functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local,?MODULE}, ?MODULE, Opts, []).

get_status() ->
    % gen_server:call(?MODULE, {get_status}).
    case whereis(?MODULE) of
        Pid when is_pid(Pid) ->
            application:get_env(?APP,{sync_schema_status},invalid);
        _ -> invalid
    end.

restart_sync() ->
    gen_server:cast(?MODULE, {restart_sync}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Opts) ->
    Ref = make_ref(),
    State1 = #state{status = set_status('initial'),
                    ref = Ref,
                    timerref = erlang:send_after(100, self(), ?RetryMsg(Ref))},
    ?OUT('$trace', "Mnesia boot srv inited", []),
    {ok, State1}.

%% ------------------------------
%% Call
%% ------------------------------

%% return current status
handle_call({get_status}, _From, #state{status=Status}=State) ->
    {reply, {ok,Status}, State};

%% other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% restarts schema synchronization
handle_cast({restart_sync}, State) ->
    ?OUT('$trace', "Mnesia boot srv sync restart...", []),
    Ref = make_ref(),
    stop_repair(),
    State1 = State#state{status = set_status('initial'),
                         ref = Ref,
                         timerref = erlang:send_after(100, self(), ?RetryMsg(Ref))},
    {noreply, State1};

%% other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% Timer retry sync
handle_info({timer_retry,Ref}, #state{ref=Ref}=State) ->
    State1 = sync_attempt(State),
    {noreply, State1};

%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
set_status(Status) ->
    application:set_env(?APP,{sync_schema_status},Status),
    Status.

%% @private
sync_attempt(State) ->
    set_status('synchronizing'),
    case {?MU:check_running(), ?BootUtils:check_schema()} of
        {true, true} -> sync_done(State);
        _ -> sync_boot(State)
    end.

%% @private
sync_boot(State) ->
    case ?Boot:boot(State) of
        {ok,State1} -> sync_done(State1);
        {X, Timeout, Reason, State1} when X==retry_after; X==retry_simple ->
            ?OUT('$info', "Mnesia boot result: retry_after ~p. ~s", [Timeout, Reason]),
            Ref = make_ref(),
            State1#state{status = set_status('waiting'),
                         ref = Ref,
                         timerref = erlang:send_after(Timeout, self(), ?RetryMsg(Ref))}
    end.

%% @private
sync_done(State) ->
    ?OUT('$info', "Mnesia schema synchronized: ~120tp", [mnesia:system_info(db_nodes)]),
    Status = set_status('running'),
    case ?CFG:schema_synchronized_function() of
        F when is_function(F,0) -> catch F();
        _ -> ok
    end,
    start_repair(),
    State#state{status = Status,
                ref = undefined,
                timerref = undefined}.

%% ------------------------------

%% @private
start_repair() ->
    ?SUPV:start_child({?RepairSrv, {?RepairSrv, start_link, [[]]}, permanent, 1000, worker, [?RepairSrv]}).

%% @private
stop_repair() ->
    case ?SUPV:get_childspec(?RepairSrv) of
        {error,not_found} -> ok;
        {ok,_} ->
            catch ?SUPV:terminate_child(?RepairSrv),
            catch ?SUPV:delete_child(?RepairSrv)
    end.



