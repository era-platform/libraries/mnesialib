
%% -------------------------
-record(state, {
    mode = '$active' :: '$active' | '$inactive',
    ref :: reference(),
    timerref :: reference(),
    monitored_nodes = [],
    nodes_to_wait = [],
    last_resync_ts = 0,
    schema_err_count = 0,
    app_err_count = 0,
    is_running = false
}).

%% -------------------------
-define(ReTimeout, 3000).

%% -------------------------
-define(RepSchema, mnesialib_repair_schema).
-define(RepIsland, mnesialib_repair_island).