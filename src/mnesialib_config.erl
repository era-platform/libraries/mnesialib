%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 02.02.2021
%%% @doc

-module(mnesialib_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([log_destination/1,
         schema_nodes/0,
         all_nodes/0,
         default_mode/0,
         schema_synchronized_function/0,
         restart_mode/0,
         create_schema_mode/0,
         create_schema_wait_nodes_timeout/0,
         nodes_sync_sequence/0]).

-export([get_env/2,
         get_all_env/0,
         set_env/2,
         unset_env/1,
         update_env/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% log_destination
log_destination(Default) ->
    get_env('log_destination', Default).

%% schema_nodes
schema_nodes() ->
    case get_env('schema_nodes',undefined) of
        undefined -> [node()];
        Nodes -> Nodes
    end.

%% all_nodes
all_nodes() ->
    case get_env('all_nodes',undefined) of
        undefined -> schema_nodes();
        Nodes -> Nodes
    end.

%% default_mode
default_mode() ->
    get_env('default_mode',disc).

%% schema_synchronized_function
schema_synchronized_function() ->
    get_env('schema_synchronized_function',undefined).

%% restart_mode
restart_mode() ->
    case ?BU:to_binary(get_env('restart_mode',undefined)) of
        <<"node">> -> node;
        node -> node;
        <<"mnesia">> -> mnesia;
        mnesia -> mnesia;
        _ -> mnesia
    end.

%% create_schema_mode
create_schema_mode() ->
    case ?BU:to_binary(get_env('create_schema_mode',undefined)) of
        <<"all_nodes">> -> all_nodes;
        all_nodes -> all_nodes;
        _ -> connected_nodes
    end.

%% create_schema_wait_nodes_timeout
create_schema_wait_nodes_timeout() ->
    case ?BU:to_binary(get_env('create_schema_wait_nodes_timeout',undefined)) of
        T when is_integer(T) -> T;
        T when is_binary(T) -> ?BU:to_int(T,20000);
        _ -> 20000
    end.

%% nodes_sync_sequence
nodes_sync_sequence() ->
    case get_env('nodes_sync_sequence',undefined) of
        F when is_function(F,0) -> F();
        [T|_]=Nodes when is_atom(T) -> Nodes;
        _ -> lists:reverse(schema_nodes())
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

get_env(Key,{fn,_}=Default) -> ?BU:get_env(?APP,Key,Default);
get_env(Key,Default) -> ?BU:get_env(?APP,Key,Default).
get_all_env() -> ?BU:get_all_env(?APP).
set_env(Key,Value) -> ?BU:set_env(?APP,Key,Value).
unset_env(Key) -> ?BU:unset_env(?APP,Key).
update_env(Opts) -> ?BU:update_env(?APP,Opts).
