%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Mnesia decorator to make distributed storage.
%%%   Application could be setup by application:set_env, by prepare(Opts), by start(Opts), by update_opts(Opts):
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'env','mns'}
%%%      schema_nodes
%%%         List of nodes containing distributed schema.
%%%         Default [node()]
%%%      schema_synchronized_function
%%%             :: function() -> ok
%%%         Function is called when distributed synchronization is finished.
%%%         Alternatives:
%%%           - call get_schema_status/0 periodically and wait for 'running'.
%%%           - use app:start() method and handle sync result (ok | error | retry_after)
%%%         Default: no action
%%%      default_mode :: disc | ram | disc_only
%%%         Mode of storage
%%%         Default: disc
%%%      restart_mode :: mnesia | node
%%%         When partitioned_network found, then unsplit starts and after it finished mnesia should be restarted.
%%%         But note, that а) tables without unsplit leave different values in replicas on conflict changes
%%%                        b) new created tables would not be synchronized if only mnesia restarts.
%%%                   So, consider to use restart_mode = node.
%%%         Default: node
%%%      create_schema_mode :: all_nodes | connected_nodes
%%%         Defines how to create schema, should node wait for connect to all of other schema nodes, or should create schema on current connected nodes.
%%%         Default: connected_nodes
%%%      create_schema_wait_nodes_timeout
%%%         Defines timeout to wait connect to all nodes before start create schema on connected nodes (used for create_schema_mode=connected_nodes).
%%%         Default: 20000
%%%      nodes_sync_sequence
%%%         Define sequence of starting sync operations for nodes. Prior node starts at earlier iteration, other pause for a while.
%%%         Used when choosing which node should start unsplit when data is splitted, which node should clear directory when schema is splitted.
%%%         Default: reverse(scheme_nodes)
%%%   Facade methods:
%%%      start/*
%%%      get_schema_status/0
%%%      make_table/*
%%%      get_table_status/1
%%%   Template services:
%%%      mnesialib_helper_schema_srv
%%%      mnesialib_helper_table_srv
%%% -------------------------------------------------------------------

-module(mnesialib).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(application).

-export([start/0,
         stop/0]).

-export([start/2, stop/1]).

%% Facade functions
-export([get_schema_status/0,
         ensure_table/4,
         drop_table/1]).

-export([size/0, size/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% ----------------------------------------
%% Starts application
%% ----------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% ----------------------------------------
%% Stops application
%% ----------------------------------------
stop() -> application:stop(?APP).

%% ====================================================================
%% Facade functions
%% ====================================================================

%% ----------------------------------------
%% Return current status of booting on stage of mnesia schema synchronization
%% ----------------------------------------
-spec get_schema_status() -> initial | synchronizing | waiting | running | invalid.
%% ----------------------------------------
get_schema_status() ->
    ?BootSrv:get_status().

%% ----------------------------------------
%% Load/Update/Create table.
%% Expected to be used by master application (active replica).
%% Opts could contain:
%%   app_name. Default 'undefined'. Name of owner application. Used to define if data should be updated and passed to update function.
%%   app_version. Default 'undefined'. Version of owner application. Used to define if data should be updated and passed to update function.
%%   type :: set | bag. Default 'set'. Type of ets storage.
%%   mode :: disc | ram | disc_only | custom. Default 'disc'. Mode of storage.
%%   mode_options :: [{disk_copies,[node()]}, {ram_copies,[node()]}, {disk_only_copies,[node()]}]
%%   index_fields :: [atom() | non_neg_integer()]. List of fields to make index.
%%   load_timeout :: non_neg_integer(). Default 10000. Timeout for loading data from table into memory before force load.
%%   load_attempt_count :: non_neg_integer(). Default 3. How many times of load_timeout would be attempted to wait_for_load.
%%   unsplit_method :: asc | select | full_merge | custom. Unsplit operation. Other dependant properties need.
%%          Default behaviour is making full copy of running island.
%%   unsplit_asc_field_index :: non_neg_integer(). Record field index to compare on unsplit. Required for unsplit_method = asc.
%%   unsplit_select_mfa :: {Module::atom(),Function::atom(),ExArgs::list()}. Required for unsplit_method = select.
%%   unsplit_custom_mfa :: {Module::atom(),Function::atom(),Args::list()}. Required for unsplit_method = custom.
%%   custom_properties :: [{Key::atom(),Value::term()}]. Custom application properties for table.
%%          Note, that dynamic references and functions should not be passed as values.
%%   update_function :: function/2 ({AppName, AppVersion}, Rec0) -> Rec1.
%%          Used to update table data when structure/version was changed.
%%          If not set then table is to be dropped and created empty.
%% ----------------------------------------
-spec ensure_table(TabName::atom(), RecName::atom(), RecFields::[atom()], Opts::map()) ->
                ok | {error,Reason::term()} | {retry_after,Timeout::non_neg_integer(),Reason::string()}.
%% ----------------------------------------
ensure_table(TabName,RecName,RecFields,Opts) ->
    ?Table:ensure_table(TabName,RecName,RecFields,Opts).

%% ----------------------------------------
%% Drops table.
%% Expected to be used by master application (active replica).
%% ----------------------------------------
drop_table(TabName) ->
    ?Table:drop_table(TabName).

%% ====================================================================
%% Application callback functions
%% ====================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_Mode, State) ->
    case lists:member(node(),?CFG:schema_nodes()) of
        false -> {error,"Current node not found in 'schema_nodes'"};
        true ->
            setup_start_ts(),
            setup_dependencies(),
            ensure_deps_started(),
            ?SUPV:start_link(State)
    end.

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% --------------------------------------
%% @private
%% setup start_utc to applicaiton's env for monitor purposes
%% --------------------------------------
setup_start_ts() ->
    application:set_env(?APP,start_utc,?BU:timestamp()).

%% --------------------------------------
%% @private
%% setup opts of dependency apps
%% --------------------------------------
setup_dependencies() ->
    % mnesia
    {ok,CWD} = file:get_cwd(),
    Defaults = [{dir, ?BU:str("~ts/Mnesia.~ts",[CWD,node()])},
                {dump_log_write_threshold, 50000},
                {dump_log_time_threshold, 180000},
                {dc_dump_limit, 1}],
    F = fun(Key,Default) ->
            case application:get_env(mnesia, dump_log_write_threshold) of
                undefined -> application:set_env(mnesia, Key, Default);
                _ -> ok
            end end,
    lists:foreach(fun({Key,Default}) -> F(Key,Default) end, Defaults),
    ok.

%% --------------------------------------
%% @private
%% starts deps applications, that are not linked to app, and to ensure
%% --------------------------------------
ensure_deps_started() ->
    {ok,_} = application:ensure_all_started(?BASICLIB, permanent),
    ok.

%% --------------------------------------
%% @private
%% Return table sizes
%% --------------------------------------
size() ->
    lists:reverse(
        lists:keysort(
            2,
            lists:map(
                fun(T) -> {T,mnesia:table_info(T,size)} end,
                mnesia:system_info(local_tables)))).

size(HigherOrEqualThan) ->
    lists:filter(
        fun({_,Size}) -> Size >= HigherOrEqualThan end,
        lists:reverse(
            lists:keysort(
                2,
                lists:map(
                    fun(T) -> {T,mnesia:table_info(T,size)} end,
                    mnesia:system_info(local_tables))))).