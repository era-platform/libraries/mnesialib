%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.04.2017
%%% @doc Mnesia utils

-module(mnesialib_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([transaction/1]).

-export([mnesia_cfg_nodes/0,
         mnesia_schema_nodes/0,
         mnesia_running_nodes/0,
         connected_nodes/0,
         %
         is_master_node/1,
         %
         check_running/0,
         check_running/1,
         %
         copy_tables/0,
         reduce_schema_to_config/0,
         clear_directory/0,
         restart_mnesia/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% transaction wrapper
%% ------------------------------
transaction(F) when is_function(F,0) ->
    case whereis(mnesia_tm) of
        undefined ->
            {aborted,{node_not_running,node()}};
        MPid ->
            TrapExit = process_flag(trap_exit,false), % #455
            CPid = self(),
            Key = <<"mnesia_tran_by_",(?BU:to_binary(pid_to_list(CPid)))/binary>>,
            Fun = fun(R) ->
                        spawn(fun() ->
                                    ?OUT('$warning', "WARNING! mnesia_tm found exited with '~120p' while transaction is active on ~120p. Kill process automatically", [R,CPid]),
                                    link(CPid),
                                    exit(self(),kill)
                              end) end,
            ?BLmonitor:append_fun(MPid,Key,Fun),
            X = mnesia:transaction(F),
            ?BLmonitor:drop_fun(MPid,Key),
            process_flag(trap_exit,TrapExit), % #455
            X
    end.

%% --------------------------------------------------------------------

%% ------------------------------
%% return actual configuration nodes to setup
%% ------------------------------
mnesia_cfg_nodes() ->
    ?CFG:schema_nodes().

%% ------------------------------
%% return actual nodes in current loaded schema
%% ------------------------------
mnesia_schema_nodes() ->
    mnesia:system_info(db_nodes).

%% ------------------------------
%% return actual running nodes from current loaded schema
%% ------------------------------
mnesia_running_nodes() ->
    mnesia:system_info(running_db_nodes).

%% ------------------------------
%% return connected nodes containing self too
%% ------------------------------
connected_nodes() ->
    [node() | nodes(connected)].

%% --------------------------------------------------------------------

%% ---------------------------------------------------
%% Returns true when current node is mnesia master node
%%  (or could be application master node), false otherwise.
%% ---------------------------------------------------
-spec is_master_node(By::undefined | pid()) -> boolean().
%% ---------------------------------------------------
is_master_node(undefined) ->
    is_master_node(get_master_node(), undefined);
is_master_node(Pid) when is_pid(Pid) ->
    is_master_node(get_master_node(), node(Pid)).

%% @private
is_master_node(Master1,Master2) ->
    CurNode = node(),
    CurNode==Master1 orelse CurNode==Master2.

%% @private
get_master_node() ->
    [Master1|_] = ?MU:mnesia_cfg_nodes(),
    Master1.

%% --------------------------------------------------------------------

%% ------------------------------
%% check if mnesia & unsplit & mnesialib apps are all running
%% ------------------------------
check_running() ->
    %Apps = [mnesia, unsplit, mnesialib],
    Apps = [mnesia, mnesialib], % TODO: without unsplit
    Names = lists:map(fun(App) -> pidname(App) end, Apps),
    lists:foldl(fun(_,false) -> false;
                   (Name,true) -> is_pid(whereis(Name))
                end, true, Names).

%% ------------------------------
%% check if individual app is running
%% ------------------------------
check_running(App) when App==mnesia; App==unsplit; App==mnesialib ->
    is_pid(whereis(pidname(App)));
check_running(App) when is_atom(App) ->
    Apps = application:which_applications(),
    case lists:keyfind(App,1,Apps) of
        false -> false;
        _ -> true
    end.

%% @private
pidname(mnesia) -> mnesia_sup;
pidname(unsplit) -> unsplit_server;
pidname(mnesialib) -> mnesialib_supv.

%% --------------------------------------------------------------------

%% ------------------------------
%% copying remote tables of schema to current node
%% ------------------------------
-spec copy_tables() -> {ok, [{Table::atom(), Result::{aborted,Reason::term()} | {atomic,ok} | term()}]}.
%% ------------------------------
copy_tables() ->
    Node = node(),
    SchemaTables = lists:sort(mnesia:system_info(tables)),
    LocalTables = lists:sort(mnesia:system_info(local_tables)),
    case SchemaTables--LocalTables of
        [] -> {ok,[]};
        ToCopy ->
            ?OUT('$info', " ~s. Mnesia copy_tables: ~120tp.", [?RoleName, ToCopy]),
            Res = [{T,add_table_copy(T, Node, disc_copies)} || T <- ToCopy],
            {ok, Res}
    end.

%% @private
add_table_copy(Tb, Node, Type) ->
    case mnesia:add_table_copy(Tb, Node, Type) of
        {aborted,_Reason}=Res ->
            % ?OUT('$force', " ~s. Mnesia table '~s' not copied (~120p).", [?RoleName, Tb, _Reason]),
            {Tb, Res};
        {atomic,ok}=Res ->
            ?OUT('$force', " ~s. Mnesia table '~s' copied.", [?RoleName, Tb]),
            {Tb, Res};
        Res ->
            {Tb, Res}
    end.

%% ------------------------------
%% reduce schema to config
%% ------------------------------
reduce_schema_to_config() ->
    case mnesia_schema_nodes() -- mnesia_cfg_nodes() of
        [] -> ok;
        ExcessNodes ->
            ?OUT('$info', " ~s. Reducing schema...", [?RoleName]),
            % Tables = mnesia:system_info(local_tables),
            Res = lists:foldl(fun(Table,Acc) ->
                                ToCheck = case ets:lookup(schema,Table) of
                                              [{schema,Table,TableSchema}] ->
                                                  Dsk = ?BU:get_by_key(disc_copies,TableSchema,[]),
                                                  Ram = ?BU:get_by_key(ram_copies,TableSchema,[]),
                                                  Dsk1 = ?BU:intersection(Dsk,ExcessNodes) /= [],
                                                  Ram1 = ?BU:intersection(Ram,ExcessNodes) /= [],
                                                  Dsk1 orelse Ram1;
                                              [] -> ?OUT('$force',"ACHTUNG! mnesia schema architecture seems to be changed!",[Table]), true;
                                              _ -> ?OUT('$force',"ACHTUNG! mnesia table 'schema' seems to be changed!",[Table]), true
                                          end,
                                case ToCheck of
                                    false -> Acc;
                                    true ->
                                        lists:foldl(fun(Node, {AccOk,AccErr}) ->
                                                        case mnesia:del_table_copy(Table,Node) of
                                                            {atomic,ok} ->
                                                                ?OUT('$trace', " ~s. Deleting '~ts' from '~ts'... ok", [?RoleName,Table,Node]),
                                                                {AccOk+1,AccErr};
                                                            {aborted,_} ->
                                                                ?OUT('$warning', " ~s. Deleting '~ts' from '~ts'... aborted", [?RoleName,Table,Node]),
                                                                {AccOk,AccErr+1}
                                                        end
                                                    end, Acc, ExcessNodes)
                                end
                             end, {0,0}, [schema]), % Tables
            ExcessNodes1 = mnesia_schema_nodes() -- mnesia_cfg_nodes(),
            case {ExcessNodes1,Res} of
                {[],_} -> ok;
                {_,{0,0}} -> error;
                {_,{_,0}} -> retry;
                {_,{0,_}} -> error;
                {_,{_,_}} -> retry
            end
    end.

%% ------------------------------
%% clear mnesia directory
%% ------------------------------
clear_directory() ->
    ?OUT('$warning', "Clear mnesia directory...", []),
    MDir = mnesia:system_info(directory),
    ?OUT('$warning', "   mnesia directory: '~ts'", [MDir]),
    % attempt 1
    catch del_dir(MDir),
    % attempt 2
    try ?BU:directory_delete(MDir)
    catch E1:R1 -> ?OUT('$warning', "Mnesia directory clear error: ~120tp", [{E1,R1}])
    end,
    mnesia:stop(),
    % attempt 3
    try ?BU:directory_delete(MDir)
    catch E2:R2 -> ?OUT('$warning', "Mnesia directory clear error: ~120tp", [{E2,R2}])
    end,
    ?OUT('$warning', "Mnesia directory cleared", []).
    % ?Rfilelib:ensure_dir(?BU:to_unicode_list(MDir) ++ "/test").

%% @private
del_dir(Path) ->
    Cmd = <<"rm -rf ",(?BU:to_binary(Path))/binary>>,
    case ?BU:cmd_exec(Cmd) of
        {0,_} -> ok;
        {_,Descr} -> {error,Descr}
    end.

%% ------------------------------
%% restarting local mnesia
%% ------------------------------
restart_mnesia() ->
    ?OUT('$warning', "MNESIA. restarting..."),
    R = ?BU:exec_fun_timered(fun() -> application:set_env(mnesia, skip_boot, true), % #333
                                      application:stop(mnesia),
                                      R = application:start(mnesia,permanent),
                                      application:unset_env(mnesia, skip_boot), % #333
                                      R
                             end, 10000),
    case R of
        ok ->
            ?OUT('$warning', "MNESIA. restart ok. loading...", []),
            case mnesia:wait_for_tables(LT=mnesia:system_info(local_tables), 5000) of
                ok -> ?OUT('$warning', "MNESIA. load ok", []);
                {timeout,TTs} ->
                    ?OUT('$warning', "MNESIA. load timeout: ~120p",[lists:sort(TTs)]),
                    ?OUT('$warning', "MNESIA. load ok: ~120p",[lists:sort(LT--TTs)]),
                    lists:foreach(fun(T) -> mnesia:force_load_table(T) end, TTs),
                    ?OUT('$warning', "MNESIA. force load ok", [])
            end;
        _ ->
            ?OUT('$warning', "MNESIA. restart: ~p", [R]),
            init:stop()
    end,
    ok.
