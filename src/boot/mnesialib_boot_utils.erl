%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.02.2021
%%% @doc

-module(mnesialib_boot_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([check_schema/0,
         application_start/1,
         application_stop/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("boot.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% =============================================
%% @private
%% check if mnesia schema already exists on current node
%% =============================================
check_schema() ->
    case {check_schema_bynodes(), check_schema_byusedir()} of
        {true, true} -> true;
        {true, false} -> true;
        {false, true} -> true;
        {false, false} -> false
    end.

%% -----
%% Checking schema existence
%% -----

%% @private
%% return true if mnesia schema contains some other nodes, false otherwise
check_schema_bynodes() ->
    MyNode = node(),
    case mnesia:system_info(db_nodes) of
        [] -> false;
        [MyNode] -> false;
        List when is_list(List) -> true
    end.

%% @private
%% return true if mnesia data directory exists and has schema, false otherwise
check_schema_byusedir() ->
    mnesia:system_info(use_dir).

%% =============================================
%% Application routines
%% =============================================

%% ------
application_start(mnesia) ->
    ?BU:exec_fun_timered(fun() -> application:start(mnesia,permanent) end, fun() -> clear_mnesia_folders(), ?BU:stop_node(0) end, 30000);
application_start(App) ->
    ?BU:exec_fun_timered(fun() -> application:start(App,permanent) end, 30000).

%% ------
application_stop(App) ->
    ?BU:exec_fun_timered(fun() -> application:stop(App) end, 10000).

%% @private
clear_mnesia_folders() ->
    ?OUT('$force', " ~s. Mnesia start tout. delete mnesia dirs",[?RoleName]),
    ?MU:clear_directory().
