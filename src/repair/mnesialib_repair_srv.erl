%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 30.03.2017
%%% @doc Implements repair mnesia service.
%%%        Issue is that after disconnect/connect some nodes consider other not running
%%%        To repair we should restart mnesia on one of "broken"" nodes.
%%%        This server starts near mnesia roles and every 3 sec check mnesia state on schema nodes.
%%%        Cases:
%%%             (A) After net disconnect db_nodes or running_db_nodes are different on nodes of schema.
%%%                 (F1) If db_nodes different, then count error times. Master 15 times then (F2) restart node, other nodes 10 times then (F3) clear data and restart node.
%%%                 (G1) If running_db_nodes different, then restart local mnesia.
%%%             (B) If configuration changed and current node is excluded from schema. Then restart node.
%%%             (C) If configuration changed and other node is excluded from schema. Then try remove it from schema,
%%%                 (C1) but if excluded node is running mnesia, then (C2) on error should skip and retry for 4 times, if still error then (C3) restart.
%%%             (D) if configuration has extra nodes that not running mnesia, so go on...
%%%             (E) If configuration has extra nodes and mnesia is running there, then something wrong in schema - remove data and restart node.


-module(mnesialib_repair_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([check_mnesia/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("repair.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local,?MODULE}, ?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Opts) ->
    State = #state{},
    gen_server:cast(self(), {start}),
    ?OUT('$trace', "Mnesia repair srv inited"),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({start}, State) ->
    State1 = case check_mode() of
                 '$active' -> set_active(State);
                 '$inactive' -> set_inactive(State)
             end,
    {noreply, State1};

handle_cast('update_cfg', #state{mode=Mode0}=State) ->
    State1 = case check_mode() of
                 Mode0 -> State;
                 '$active' -> set_active(State);
                 '$inactive' -> set_inactive(State)
             end,
    {noreply, State1};

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

% Timer
handle_info({timer_msg_active,Ref}, #state{ref=Ref,mode='$active'}=State) ->
    State1 = check_mnesia(State),
    Ref1 = make_ref(),
    State2 = State1#state{ref=Ref1,
                          timerref=erlang:send_after(?ReTimeout,self(),{timer_msg_active,Ref1})},
    {noreply, State2};

handle_info({timer_msg_inactive,Ref}, #state{ref=Ref,mode='$inactive'}=State) ->
    State1 = case check_mode() of
                 '$active' -> set_active(State);
                 '$inactive' ->
                     Ref1 = make_ref(),
                     State#state{ref = Ref1,
                                 timerref=erlang:send_after(60000,self(),{timer_msg_inactive,Ref1})}
             end,
    {noreply, State1};

% Down of node
handle_info({nodedown,Node}, #state{monitored_nodes=Monitored}=State) ->
    ?OUT('$warning', "MNESIA. Node '~s' found down", [Node]),
    State1 = State#state{monitored_nodes=Monitored--[Node]},
    {noreply, State1};

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================


%% when 1 server in group (schema), then passive
%% when several servers in group (schema), then active
check_mode() ->
    Node = node(),
    case {mnesia:system_info('db_nodes'), ?CFG:schema_nodes()} of
        {[Node],[Node]} -> '$inactive';
        _ -> '$active'
    end.

%% @private
set_active(#state{timerref=TimerRef}=State) ->
    ?OUT('$info', "MNESIA. RepairSrv activate", []),
    ?BU:cancel_timer(TimerRef),
    Ref1 = make_ref(),
    State#state{mode = '$active',
                ref = Ref1,
                timerref=erlang:send_after(1000,self(),{timer_msg_active,Ref1})}.

%% @private
set_inactive(#state{timerref=TimerRef}=State) ->
    ?OUT('$info', "MNESIA. RepairSrv inactivate", []),
    ?BU:cancel_timer(TimerRef),
    Ref1 = make_ref(),
    State#state{mode = '$inactive',
                ref = Ref1,
                timerref=erlang:send_after(60000,self(),{timer_msg_inactive,Ref1})}.

% ----------------------------------------------
% repair after reconnect
%  (mnesia sometimes loose link to connected nodes from schema)
% ----------------------------------------------

check_mnesia(State) ->
    check_mnesia_0(State#state{is_running=false}).

%% check if node is open
check_mnesia_0(State) ->
    %case ?BU:is_open_node() of
    %    false -> State; % TODO: check if hidden nodes could run distributed mnesia
    %    _ -> check_mnesia_1a(State)
    %end.
    check_mnesia_1a(State).

%% check if mnesia is running
check_mnesia_1a(State) ->
    case catch mnesia:system_info(running_db_nodes) of
        {'EXIT',Reason} ->
            ?OUT('$error', "MNESIA. check running nodes error: ~120p", [Reason]),
            State;
        [] ->
            % only if something wrong in running_db_nodes. In idle happy way is not executed
            check_mnesia_1b([],State);
        RunningDbNodes ->
            check_mnesia_1b(RunningDbNodes,State)
    end.

%% check mnesia apps are started (basiclib start is earlier than mnesia)
check_mnesia_1b(RunningDbNodes, #state{app_err_count=ErrC}=State) ->
    case ?MU:check_running() of
        false ->
            case ErrC < 3 of
                true ->
                    ?OUT('$warning', "MNESIA. found mnesia down. wait..."),
                    State#state{app_err_count=ErrC+1};
                false ->
                    ?OUT('$warning', "MNESIA. found mnesia down. restarting..."),
                    ?MU:restart_mnesia(),
                    State#state{app_err_count=0}
            end;
        true ->
            % Apps have been just checked
            case ?RepSchema:check_schema_noapps(RunningDbNodes, State) of
                {ok,State1} ->
                    check_mnesia_2(State1#state{is_running=true,
                                                app_err_count=0});
                {skip,State1} -> State1;
                {stop,State1} -> State1
            end
    end.

%% filter if nothing changed in connect/disconnect on current db_nodes (economy)
check_mnesia_2(#state{monitored_nodes=Monitored}=State) ->
    Node = node(),
    {M,F,A} = {mnesia,system_info,[db_nodes]},
    Schema = erlang:apply(M,F,A),
    case (Schema--[Node])--Monitored of
        [] ->
            Status = application:get_env(?APP, unsplit_status),
            case Monitored--mnesia:system_info(running_db_nodes) of
                [] when Status==undefined -> State;
                _ -> check_mnesia_3(State)
            end;
        _ -> check_mnesia_3(State)
    end.

%% filter db_nodes, where schema is differ (correct)
%% check db_nodes
check_mnesia_3(#state{schema_err_count=ErrC}=State) ->
    % CASE (A)
    State1 = State#state{last_resync_ts=?BU:timestamp()},
    Node = node(),
    {M,F,A} = {mnesia,system_info,[db_nodes]},
    LocalSchema = erlang:apply(M,F,A),
    {Res,ErrNodes} = ?BLmulticall:map_result_rpc_multicall(?BLmulticall:call_direct(LocalSchema--[Node], {M, F, A}, 5000)),
    [Master|_] = ?MU:mnesia_cfg_nodes(),
    Flogdif = fun() ->
                DifNodes = lists:foldl(fun({N,S},Acc) when S/=LocalSchema -> [N|Acc]; (_,Acc) -> Acc end, [], lists:zip(LocalSchema--[Node|ErrNodes],Res)),
                ?OUT('$warning', "MNESIA. sync problem found. Schema is different. ~n\tschema=~120tp, ~n\tother: ~120tp, ~n\tdif: ~120tp", [LocalSchema, lists:zip(LocalSchema--[Node|ErrNodes], Res), DifNodes]),
                DifNodes
           end,
    case lists:all(fun(R) -> R==LocalSchema end, Res) of
        false when ErrC > 15, Node==Master ->
            % CASE (F2)
            Flogdif(),
            ?OUT('$warning', "MNESIA. sync problem found. Restart node."),
            ?BU:stop_node(0),
            State;
        false when ErrC > 10, Node==Master ->
            % CASE (F3)
            Flogdif(),
            ?OUT('$warning', "MNESIA. sync problem found. Clear data directory and restart node..."),
            ?MU:clear_directory(),
            ?BU:stop_node(0),
            State;
        false ->
            % CASE (F1)
            DifNodes = Flogdif(),
            Correct = (LocalSchema--[Node|ErrNodes])--DifNodes,
            check_mnesia_4(Correct, LocalSchema, State1#state{schema_err_count=ErrC+1});
        true ->
            check_mnesia_4(LocalSchema--[Node|ErrNodes], LocalSchema, State1#state{schema_err_count=0})
    end.

%% check islands and copy new tables
check_mnesia_4(ActiveDbNodes, LocalSchema, State) ->
    case ?RepIsland:check_island(ActiveDbNodes, LocalSchema, State) of
        {skip,State1} -> State1;
        {ok,State1} ->
            ?MU:copy_tables(),
            State1
    end.
