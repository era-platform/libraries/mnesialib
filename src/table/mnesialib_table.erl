%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.02.2021
%%% @doc Ensure table functionality
%%%      Get

-module(mnesialib_table).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([ensure_table/4,
         drop_table/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------
%% Ensure table with selected structure and properties
%% Opts could contain:
%%   app_name. Default 'undefined'. Name of owner application. Used to define if data should be updated and passed to update function.
%%   app_version. Default 'undefined'. Version of owner application. Used to define if data should be updated and passed to update function.
%%   type :: set | bag | ordered_set. Default 'set'. Type of ets storage.
%%   load_timeout :: non_neg_integer(). Default 10000. Timeout for loading data from table into memory before force load.
%%   load_attempt_count :: non_neg_integer(). Default 3. How many times of load_timeout would be attempted to wait_for_load.
%%   unsplit_method :: asc | select | full_merge | custom. Unsplit operation. Other dependant properties need.
%%          Default behaviour is making full copy of running island.
%%   unsplit_asc_field_index :: non_neg_integer(). Record field index to compare on unsplit. Required for unsplit_method = asc.
%%   unsplit_select_mfa :: {Module::atom(),Function::atom(),ExArgs::list()}. Required for unsplit_method = select.
%%   unsplit_custom_mfa :: {Module::atom(),Function::atom(),Args::list()}. Required for unsplit_method = custom.
%%   custom_properties :: [{Key::atom(),Value::term()}]. Custom application properties for table.
%%          Note, that dynamic references and functions should not be passed as values.
%%   update_function :: function/2 ({AppName, AppVersion}, Rec0) -> Rec1.
%%          Used to update table data when structure/version was changed.
%%          If not set then table is to be dropped and created empty.
%% ----------------------------------------
-spec ensure_table(TabName::atom(), RecName::atom(), RecFields::[atom()], Opts::map()) ->
            ok | {error,Reason::string()} | {retry_after,Timeout::non_neg_integer(),Reason::string()}.
%% ----------------------------------------
ensure_table(TabName,RecName,RecFields,Opts) ->
    ?OUT('$debug',"Mnesia ensure table '~ts': '~ts'~n\tFields: ~500tp~n\tOpts: ~120tp", [TabName,RecName,RecFields,Opts]),
    Ftrace = ?BLtrace:start(60000),
    FunRes = fun(Res) ->
                    ?OUT('$trace',"Mnesia ensure table '~ts' trace: ~300p", [TabName,?BLtrace:stop(Ftrace,100)]),
                    case Res of
                        ok ->
                            ?OUT('$debug',"Mnesia ensure table '~ts' -> ok", [TabName]),
                            ok;
                        {retry_after,_,_}=Retry ->
                            ?OUT('$debug',"Mnesia ensure table '~ts' -> ~120tp", [TabName,Retry]),
                            Retry;
                        {error,_}=Err ->
                            ?OUT('$debug',"Mnesia ensure table '~ts' -> ~120tp", [TabName,Err]),
                            Err
                    end end,
    try
        case check_mnesia_ready() of
            ok ->
                Ftrace("B"),
                case is_table_exist(TabName) of
                    true ->
                        Ftrace("C"),
%%                        set_master_nodes(TabName),
                        case ?TableLoad:load_table(TabName,Opts) of
                            true ->
                                Ftrace("D"),
                                case ?TableStructure:check_table_structure(TabName, RecName, RecFields, Opts) of
                                    ok ->
                                        Ftrace("E"),
                                        case ?TableProps:is_app_version_changed(TabName, RecName, RecFields, Opts) of
                                            false ->
                                                Ftrace("F"),
                                                FunRes(?TableProps:update_table_properties(TabName, RecName, RecFields, Opts));
                                            true ->
                                                Ftrace("F2"),
                                                FunRes(?TableUpdate:update_table_data(TabName, RecName, RecFields, Opts))
                                        end;
                                    {error,type} ->
                                        Ftrace("E1"),
                                        FunRes(?TableUpdate:recreate_table(TabName, RecName, RecFields, Opts));
                                    {error,mode} ->
                                        Ftrace("E2"),
                                        FunRes(?TableUpdate:recreate_table(TabName, RecName, RecFields, Opts));
                                    {error,_} ->
                                        Ftrace("E3"),
                                        FunRes(?TableUpdate:update_table_structure(TabName, RecName, RecFields, Opts))
                                end;
                            T ->
                                Ftrace("D2"),
                                FunRes(T)
                        end;
                    false ->
                        Ftrace("C2"),
                        FunRes(?TableCreate:create_table(TabName, RecName, RecFields, Opts))
                end;
            T ->
                Ftrace("B2"),
                FunRes(T)
        end
    catch
        throw:R -> FunRes(R); % normal way
        error:R:ST -> ?OUT('$force',"~120p (~120p)",[R,ST]), FunRes({error,R});
        exit:R -> FunRes({error,{exited,R}})
    end.

%% ----------------------------------------
%% Drops table
%% ----------------------------------------
drop_table(TabName) ->
    try
        case check_mnesia_ready() of
            ok ->
                case is_table_exist(TabName) of
                    true -> ?TableDelete:delete_table(TabName);
                    false -> {error,not_exists}
                end;
            _ -> {error,not_ready}
        end
    catch
        error:R -> {error,R};
        exit:R -> {error,{exited,R}}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%%%% ---------------------------------------
%%%% @private
%%set_master_nodes(TabName) ->
%%    % to avoid automatic table copying from less relevant table copies.
%%    % to ensure fast loading on start of application.
%%    % to have time to unsplit.
%%    % it is stored locally (not distributed property)
%%    mnesia:set_master_nodes(TabName, [node()]).

%% ---------------------------------------
%% @private
check_mnesia_ready() ->
    {_,Running}=lists:keyfind(running,1,application:info()),
    case lists:keyfind(mnesia,1,Running) of
        false -> {retry_after, ?TIMEOUT_WaitMaster, "Wait for schema's master node to create tables"};
        {_,_} ->
            BootSrvPid = whereis(?BootSrv),
            case ?BootSrv:get_status() of
                'running' -> ok;
                'invalid' -> {error, "Boot server not found"};
                _ when not is_pid(BootSrvPid) -> {error, "Boot server not found"};
                _ -> {retry_after, ?TIMEOUT_WaitBoot, "Boot server not finished schema synchronization"}
            end end.

%% ---------------------------------------
%% @private
is_table_exist(TabName) ->
    case catch mnesia:table_info(TabName, load_node) of
        {'EXIT', {aborted,{no_exists,_,_}}} -> false;
        _ -> true
    end.
