
%% -------------------------
-record(state, {
    status = 'initial' :: initial | synchronizing | waiting | running,
    ref,
    timerref
}).

%% -------------------------
-define(TIMEOUT_WAITNODES, 10000).
-define(TIMEOUT_WAITMASTER, 10000).
-define(TIMEOUT_ERRORAPP, 10000).
-define(TIMEOUT_SIMPLE, 2000).
-define(TIMEOUT_RETRY, 10).
-define(EXPANDSCHEMA_KEY, <<"37B6A2">>).

%% -------------------------
-define(BootRebuildSchema, mnesialib_boot_schema_rebuild).
-define(BootMakeSchema, mnesialib_boot_schema_make).
-define(BootSchemaExpandRemote, mnesialib_boot_schema_expand_remote).
-define(BootSchemaCreate, mnesialib_boot_schema_create).
-define(BootUtils, mnesialib_boot_utils).