%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.02.2021
%%% @doc Check table structure routines

-module(mnesialib_table_structure).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([check_table_structure/4]).
-export([a/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

a() -> asdf.

%% --------------------------------------
%% return ok if structure of table was not changed and {error,Details} otherwise
%% --------------------------------------
-spec check_table_structure(TabName::atom(), RecName::atom(), RecFields::[atom()], Opts :: map()) ->
                ok | {error, record_fields | record_name | record}.
%% --------------------------------------
check_table_structure(TabName, RecName, RecFields, Opts) ->
    % if version is the same or unknown, then table should be erased and built again.
    case check_table_mode(TabName, Opts) of
        ok -> check_table_type(TabName, RecName, RecFields, Opts);
        Err -> Err
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
check_table_mode(TabName, Opts) ->
    Disc = lists:sort(mnesia:table_info(TabName, disc_copies)),
    Ram = lists:sort(mnesia:table_info(TabName, ram_copies)),
    DiscOnly = lists:sort(mnesia:table_info(TabName, disc_only_copies)),
    DiscLen = length(Disc),
    RamLen = length(Ram),
    DiscOnlyLen = length(DiscOnly),
    case maps:get(mode, Opts, ?CFG:default_mode()) of
        disc when DiscLen>0, RamLen==0, DiscOnlyLen==0 -> ok;
        disc -> {error,mode};
        ram  when DiscLen==0, RamLen>0, DiscOnlyLen==0 -> ok;
        ram -> {error,mode};
        disc_only when DiscLen==0, RamLen==0, DiscOnlyLen>0 -> ok;
        disc_only -> {error,mode};
        custom ->
            TableOpts = maps:get(mode_options, Opts, []),
            case lists:sort(?BU:get_by_key(disc_copies,TableOpts,[])) == Disc
                 andalso lists:sort(?BU:get_by_key(ram_copies,TableOpts,[])) == Ram
                 andalso lists:sort(?BU:get_by_key(disc_only_copies,TableOpts,[])) == DiscOnly
            of
                true -> ok;
                false -> {error,mode}
            end
    end.

%% @private
check_table_type(TabName, RecName, RecFields, Opts) ->
    TableType = maps:get(type, Opts, set),
    case ets:info(TabName,type) of
        TableType -> check_table_record(TabName, RecName,RecFields);
        _ -> {error,type}
    end.

%% @private
check_table_record(TabName, RecName, RecFields) ->
    case {mnesia:table_info(TabName,record_name), mnesia:table_info(TabName,attributes)} of
        {RecName,RecFields} -> ok;
        {RecName,_} -> {error,record_fields};
        {_,RecFields} -> {error,record_name};
        {_,_} -> {error,record}
    end.