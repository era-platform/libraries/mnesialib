%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.02.2021
%%% @doc Finish check/repair operations. Schema is ok. Check running_db_nodes, and apply unsplit/restart.

-module(mnesialib_repair_island).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([check_island/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("repair.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

check_island(ActiveDbNodes, LocalSchema, State) ->
    do_check_island_1(ActiveDbNodes, LocalSchema, State).

%% ====================================================================
%% Internal process functions
%% ====================================================================

%% filter db_nodes where mnesia is fully running (correct)
do_check_island_1(ActiveDbNodes, LocalSchema, State) ->
    {M,F,A} = {?MU,check_running,[]},
    {Res,ErrNodes} = ?BLmulticall:map_result_rpc_multicall(?BLmulticall:call_direct(ActiveDbNodes, {M, F, A}, 5000)),
    ActiveDbNodes1 = lists:filtermap(fun({N,true}) -> {true,N};
                                        ({_,false}) -> false
                                     end, lists:zip(ActiveDbNodes--ErrNodes,Res)),
    do_check_island_2(ActiveDbNodes1, LocalSchema, State).

%% check if running nodes are the same
%% check running_db_nodes
do_check_island_2(ActiveDbNodes, LocalSchema, State) ->
    % CASE (G)
    Node = node(),
    State1 = monitor_nodes(ActiveDbNodes,State),
    {M,F,A} = {mnesia,system_info,[running_db_nodes]},
    Rloc = erlang:apply(M,F,A),
    {Res,ErrNodes} = ?BLmulticall:map_result_rpc_multicall(MulticallRes=?BLmulticall:call_direct(ActiveDbNodes, {M, F, A}, 5000)),
    IsUnsplitActive = is_unsplit_active(State),
    FoundEverywhere = lists:all(fun(R) -> lists:member(Node,R) end, Res),
    NotSyncNodes = (ActiveDbNodes--ErrNodes)--[Node|Rloc],
    case { IsUnsplitActive, FoundEverywhere, NotSyncNodes } of
        {false,true,[]} ->
            application:unset_env(?APP, unsplit_status),
            {ok,State1};
        {true,true,[]} ->
            ?OUT('$warning', "MNESIA Unsplit. Sync restored! ~n\tActive nodes: ~120tp~n\tStopped nodes: ~120tp",[ActiveDbNodes, mnesia:system_info(db_nodes) -- Rloc]),
            application:unset_env(?APP, unsplit_status),
            {ok,State1};
        {true,_,Ns} ->
            _NsRes = lists:map(fun(Nx) -> lists:keyfind(Nx,1,MulticallRes) end, Ns),
            on_wait_followers(State1);
        {_,_,[_|_]=Ns} ->
            % CASE (G1)
            ?OUT('$warning', "MNESIA Unsplit. Sync problem found! ~n\tNodes ~120tp is out of local running list. ~n\tSchema: ~1000tp, ~n\trunning(loc): ~1000tp.", [Ns,LocalSchema,Rloc]),
            NsRes = lists:map(fun(Nx) -> lists:keyfind(Nx,1,MulticallRes) end, Ns),
            sync_partitioned_network(NsRes,State1);
        {_,false,[]=Ns} ->
            % CASE (G2)
            ResNodes = ActiveDbNodes--ErrNodes,
            Facc = fun(_,{err,_,_,_}=Acc) -> Acc;
                      (R,{ok,I,#state{nodes_to_wait=NW}=AccState}) ->
                            case lists:member(Node,R) of
                                true -> {ok,I+1,AccState};
                                false ->
                                    N = lists:nth(I,ResNodes),
                                    case lists:member(N,NW) of
                                        true -> {err,N,R,AccState};
                                        false -> {ok,I+1,AccState#state{nodes_to_wait=[N|NW]}}
                                    end end end,
            case lists:foldl(Facc, {ok,1,State1}, Res) of
                {ok,_,State2} ->
                    {skip,State2};
                {err,N,Rrem,State2} ->
                    ?OUT('$warning', "MNESIA Unsplit. Sync problem found. Node '~ts'. ~n\tSchema: ~1000tp, ~n\trunning(loc): ~1000tp, ~n\trunning(rem): ~1000tp.", [N,LocalSchema,Rloc,Rrem]),
                    NsRes = lists:map(fun(Nx) -> lists:keyfind(Nx,1,MulticallRes) end, Ns),
                    sync_partitioned_network(NsRes,State2)
            end end.

%% ====================================================================
%% Internal private functions
%% ====================================================================

%% @private
%% start monitor nodes (optimize)
monitor_nodes(Nodes,State) ->
    lists:foldl(fun(N,#state{monitored_nodes=Monitored}=AccState) ->
                        case lists:member(N,Monitored) of
                            true -> AccState;
                            false ->
                                ?OUT('$trace', "MNESIA Unsplit. Monitor node '~s'", [N]),
                                erlang:monitor_node(N,true),
                                AccState#state{monitored_nodes=[N|Monitored]}
                        end
                end, State, Nodes).

%% @private
sync_partitioned_network([],State) ->
    do_restart_mnesia(State);
sync_partitioned_network(OtherIslandsNodeRes,State) ->
    % count how many iterations to skip before push unsplit.
    %   let less priority nodes such as master to perform operation and restart only if other are inactive.
    SyncNodes = ?CFG:nodes_sync_sequence(),
    Position = ?BU:position(node(),SyncNodes),
    SkipIterations = (Position - 1) * 2,
    ?OUT('$warning',"MNESIA Unsplit. Election -> ~p...",[Position]),
    application:set_env(?APP, unsplit_status,{'wait',{SkipIterations,OtherIslandsNodeRes}}),
    on_wait_followers(State).

%% @private
is_unsplit_active(_State) ->
    case application:get_env(?APP, unsplit_status) of
        {_,{'wait',_}} -> true;
        _ -> false
    end.

%% @private
on_wait_followers(State) ->
    ?LOG('$info',"MNESIA Unsplit. Wait followers... (~120tp)",[application:get_env(?APP, unsplit_status)]),
    case application:get_env(?APP, unsplit_status) of
        % no one node has locked sync, skip attempts counter is +
        {_,{'wait',{SkipIterations,OtherIslandsNodeRes}}} when SkipIterations > 0 ->
            application:set_env(?APP, unsplit_status, {'wait',{SkipIterations-1,OtherIslandsNodeRes}}),
            {skip,State};
        _ ->
            do_restart_mnesia(State)
    end.

%% @private
do_restart_mnesia(State) ->
    case ?CFG:restart_mode() of
        mnesia ->
            ?OUT('$warning', "Attempting to restart mnesia...",[]),
            application:unset_env(?APP, unsplit_status),
            ?MU:restart_mnesia(),
            {skip,State};
        node ->
            ?OUT('$warning', "Attempting to restart node."),
            ?BU:stop_node(0),
            {skip,State}
    end.