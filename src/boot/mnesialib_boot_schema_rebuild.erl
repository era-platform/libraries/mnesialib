%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.02.2021
%%% @doc Boot rebuild schema routine.
%%%      Used if schema exists on node on start.

-module(mnesialib_boot_schema_rebuild).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([rebuild_schema/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("boot.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

rebuild_schema(State) ->
    case rebuild_schema_0(State) of
        {ok,State1} ->
            check_copy_tables(),
            {ok,State1};
        T -> T
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% =============================================
%% @private
%% check if existing mnesia schema has excess nodes
%%   store table data, drop mnesia folder, create new schema, restore tables from folder
%% =============================================
rebuild_schema_0(State) ->
    ?OUT('$trace', " ~s. -> Rebuild mnesia schema...", [?RoleName]),
    case ?MU:mnesia_schema_nodes() -- ?MU:mnesia_cfg_nodes() of
        [] -> {ok,State};
        ExcessNodes ->
            ?OUT('$warning', " ~s. Mnesia schema found excess nodes: ~120p", [?RoleName,ExcessNodes]),
            case ?MU:reduce_schema_to_config() of
                ok -> {ok,State};
                retry -> {retry_after, ?TIMEOUT_SIMPLE, "Retry remove excess nodes", State};
                error ->
                    Tables = mnesia:system_info(local_tables),
                    DbNodes = mnesia:system_info(db_nodes),
                    rebuild_schema_1({Tables,DbNodes}, State)
            end
    end.
%%
rebuild_schema_1(Info,State) ->
    ?BootUtils:application_stop(mnesia),
    CfgNodes = ?MU:mnesia_cfg_nodes(),
    ConnectedNodes = ?MU:connected_nodes(),
    rebuild_schema_2({CfgNodes,ConnectedNodes},Info,State).
%%
rebuild_schema_2({CfgNodes,ConnectedNodes}, Info, State) ->
    case ?BU:intersection_in_order(CfgNodes,ConnectedNodes) of
        [] -> {retry_after, ?TIMEOUT_WAITNODES, "Expects other nodes to connect (a)", State};
        Nodes -> rebuild_schema_3(Nodes, Info, State)
    end.
%%
rebuild_schema_3([Master|_]=Nodes, {Tables,_DbNodes}=Info, State) ->
    case Master == node() of
        false ->
            clear_directory(Tables),
            {retry_after, ?TIMEOUT_WAITMASTER, "Wait master node " ++ ?BU:to_list(Master) ++ " to rebuild mnesia schema", State};
        true ->
            rebuild_schema_4(Nodes, Info, State)
    end.
%%
rebuild_schema_4(Nodes, {Tables,DbNodes}, State) ->
    store_tables_data("temp/mnesia_stored_data", Tables),
    clear_directory(DbNodes),
    ?BootSchemaCreate:create_schema(Nodes, State).

%% @private
store_tables_data(_,[]) -> ok;
store_tables_data(_,[schema]) -> ok;
store_tables_data(Dir, Tables) ->
    ?OUT('$force', " ~s. Store mnesia tables data...", [?RoleName]),
    ?BootUtils:application_start(mnesia),
    ?BU:directory_delete(Dir),
    Ffile = fun(Tab) -> ?BU:str("~s/~s.bak", [Dir,Tab]) end,
    lists:foreach(fun(schema) -> ok;
                     (Tab) ->
                        File = Ffile(Tab),
                        mnesia:force_load_table(Tab),
                        ?Rfilelib:ensure_dir(File),
                        Res = case catch ets:tab2file(Tab, File) of T -> T end,
                        ?OUT('$force', " ~s. Mnesia store '~s' to '~s': ~120p", [?RoleName,Tab,File,Res])
                  end, Tables),
    ?BootUtils:application_stop(mnesia).

%% ------------------------------------
%% @private
%% ------------------------------------
clear_directory([]) -> ok; % already empty
clear_directory(_) ->
    ?OUT('$force', " ~s. Clear directory...", [?RoleName]),
    ?MU:clear_directory().

%% ------------------------------------
%% @private
%% Copy tables to expanded node (running on slave)
%% ------------------------------------
check_copy_tables() ->
    ?OUT('$force', " ~s. Mnesia check_copy_tables", [?RoleName]),
    ?MU:copy_tables().