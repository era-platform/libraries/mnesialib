
%% ====================================================================
%% Define modules
%% ====================================================================

-define(APP, mnesialib).
-define(SUPV, mnesialib_supv).
-define(BASICLIB, basiclib).

-define(CFG, mnesialib_config).
-define(MU, mnesialib_utils).

-define(WORKERSUPV, mnesialib_worker_supv).
-define(WORKER, mnesialib_worker_srv).

-define(Boot, mnesialib_boot_expand).
-define(BootSrv, mnesialib_boot_srv).

-define(Table, mnesialib_table).
-define(TableLoad, mnesialib_table_load).
-define(TableCreate, mnesialib_table_create).
-define(TableUpdate, mnesialib_table_update).
-define(TableDelete, mnesialib_table_delete).
-define(TableStructure, mnesialib_table_structure).
-define(TableProps, mnesialib_table_properties).
-define(TableU, mnesialib_table_utils).

-define(RepairSrv, mnesialib_repair_srv).

-define(Unsplit, mnesialib_unsplit).


-define(BU, basiclib_utils).
-define(Rfilelib, basiclib_wrapper_filelib).
-define(Rfile, basiclib_wrapper_file).
-define(BLlog, basiclib_log).
-define(BLping, basiclib_ping).
-define(BLrpc, basiclib_rpc).
-define(BLmulticall, basiclib_multicall).
-define(BLmonitor, basiclib_monitor_srv).
-define(BLopts, basiclib_srv).
-define(BLstore, basiclib_store).
-define(BLtrace, basiclib_trace).

%% ====================================================================
%% Define other
%% ====================================================================

-define(RoleName, "BOOT").

-define(TIMEOUT_WaitBoot, 5000).
-define(TIMEOUT_WaitReplica, 10000).
-define(TIMEOUT_WaitMaster, 10000).
-define(TIMEOUT_Retry, 1000).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {env,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level,?CFG:log_destination(?LOGFILE),{Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level,?CFG:log_destination(?LOGFILE),Text)).

-define(OUT(Text), ?BLlog:out(Text)).
-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level,?CFG:log_destination(?LOGFILE),{Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level,?CFG:log_destination(?LOGFILE),Text)).

-define(OUTC(Level,Fmt,Args), ?BLlog:out(Fmt,Args)).
-define(OUTC(Level,Text), ?BLlog:out(Text)).