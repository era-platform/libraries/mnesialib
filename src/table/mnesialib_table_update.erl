%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.02.2021
%%% @doc

-module(mnesialib_table_update).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    recreate_table/4,
    update_table_data/4,
    update_table_structure/4
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------------------
%% Delete old and create new table and fill updated data by function in opts.
%% Keep new table empty if function not defined
%% ------------------------------------------
recreate_table(TabName, RecName, RecFields, Opts) ->
    do_recreate_table(TabName, RecName, RecFields, Opts).

%% ------------------------------------------
%% Update table data in the same structure by function in opts.
%% Update table properties after it.
%% Drop table and create new if function not defined.
%% ------------------------------------------
update_table_data(TabName, RecName, RecFields, Opts) ->
    do_transform_table(TabName, RecName, RecFields, Opts).

%% ------------------------------------------
%% Create new table and fill updated data by function in opts.
%% Keep new table empty if function not defined
%% ------------------------------------------
update_table_structure(TabName, RecName, RecFields, Opts) ->
    do_transform_table(TabName,RecName,RecFields,Opts).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------
%% @private
do_recreate_table(TabName, RecName, RecFields, Opts) ->
    case catch ?TableDelete:delete_table(TabName) of
        {'EXIT',_} -> {retry_after,?TIMEOUT_Retry,"Exception was caught"};
        {retry_after,_,_}=Retry -> Retry;
        ok ->
            ?TableCreate:create_table(TabName, RecName, RecFields, Opts)
    end.

%% --------
%% @private
do_transform_table(TabName, RecName, RecFields, Opts) ->
    CurrentRecName = ?TableProps:get_record_name(TabName),
    CurrentAppOwner = case ?TableProps:get_app_owner(TabName) of
                          {_,_}=T -> T;
                          _ -> undefined
                      end,
    case maps:get(update_function, Opts, undefined) of
        Fun when is_function(Fun,2), CurrentAppOwner/=undefined, RecName==CurrentRecName ->
            del_table_indexes(TabName),
            {PrevAppName,PrevAppVersion} = CurrentAppOwner,
            case mnesia:transform_table(TabName, fun(Value) -> Fun({PrevAppName,PrevAppVersion}, Value) end, RecFields, RecName) of
                {aborted,Reason} -> {retry_after,?TIMEOUT_Retry,Reason};
                {atomic,ok} ->
                    add_table_indexes(TabName,RecName,RecFields,Opts),
                    ?TableProps:update_table_properties(TabName, RecName, RecFields, Opts)
            end;
        _ ->
            recreate_table(TabName, RecName, RecFields, Opts)
    end.

%% @private
del_table_indexes(TabName) ->
    RecFields0 = mnesia:table_info(TabName,attributes),
    lists:foreach(fun(A) ->
                        catch mnesia:del_table_index(TabName,A)
                  end, RecFields0).

%% @private
add_table_indexes(TabName,_RecName,_RecFields,Opts) ->
    IndexFields = maps:get(index_fields,Opts,[]),
    lists:foreach(fun(A) ->
                        mnesia:add_table_index(TabName,A)
                  end, IndexFields).