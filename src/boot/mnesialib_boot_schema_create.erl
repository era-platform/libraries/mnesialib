%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.02.2021
%%% @doc

-module(mnesialib_boot_schema_create).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([create_schema/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("boot.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------------
%% @private
%% Creates schema on connected nodes,
%%   if error - retry operation in few milliseconds
%% ------------------------------------
create_schema(Nodes, State) ->
    ?OUT('$force', " ~s. Mnesia create_schema", [?RoleName]),
    timer:sleep(500),
    CurrentNode = node(),
    case mnesia:create_schema(Nodes) of
        ok ->
            ?BootUtils:application_start(mnesia),
            {ok, State};
        {error, {CurrentNode, {already_exists,_}}} ->
            ?BootUtils:application_start(mnesia),
            {ok, State};
        {error, _}=E ->
            ?OUT('$force', " ~s. Mnesia create schema error: ~120p", [?RoleName,E]),
            {retry_after, ?TIMEOUT_RETRY, "Expects other nodes to stop mnesia", State}
    end.
