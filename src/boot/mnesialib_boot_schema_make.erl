%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.02.2021
%%% @doc Boot make schema routine.
%%%      Used if no schema is found on node on start.

-module(mnesialib_boot_schema_make).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([make_schema/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("boot.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

make_schema(State) ->
    make_schema_1(State).

%% ====================================================================
%% Internal process functions
%% ====================================================================

%% =============================================
%% Attemts to make schema or wait for required conditions
%% =============================================
make_schema_1(State) ->
    %{<<"schema">>, [<<"a">>, <<"b">>, <<"c">>]},
    ?OUT('$trace', " ~s. -> Make mnesia schema", [?RoleName]),
    Node = node(),
    CfgNodes = ?MU:mnesia_cfg_nodes(),
    case lists:member(Node,CfgNodes) of
        false -> {retry_after, ?TIMEOUT_WAITNODES, "Current node not found in configuration", State};
        true ->
            ConnectedNodes = ?MU:connected_nodes(),
            CfgConnectedNodes = ?BU:intersection_in_order(CfgNodes,ConnectedNodes),
            PingCfgConnectedNodes = [Node|lists:filtermap(fun ping_mapresult/1, ping(CfgConnectedNodes--[Node], []))],
            make_schema_2({CfgNodes, PingCfgConnectedNodes}, State)
    end.
%%
make_schema_2({CfgNodes, Nodes}, State) ->
    case try_request_schema(Nodes--[node()], State) of
        {ok, _FNode} -> {ok, State};
        retry_after -> {retry_after, ?TIMEOUT_WAITNODES, "Expects other nodes on request_schema", State};
        false ->
            Mode = ?CFG:create_schema_mode(),
            WaitTimeout = ?CFG:create_schema_wait_nodes_timeout(),
            case CfgNodes -- Nodes of
                [] ->
                    make_schema_3({CfgNodes}, State);
                _ when Mode=='connected_nodes' ->
                    NowTS = ?BU:timestamp(),
                    Key = {?MODULE,?FUNCTION_NAME,wait_nodes_expire_ts},
                    case ?BLstore:find_t(Key) of
                        false when WaitTimeout>0 ->
                            ?BLstore:store_t(Key,NowTS+WaitTimeout,WaitTimeout+30000),
                            {retry_after, ?TIMEOUT_WAITNODES, "Expects other nodes to connect for 1 minute...", State};
                        {_,TS} when TS >= NowTS ->
                            {retry_after, ?TIMEOUT_WAITNODES, ?BU:str("Expects other nodes to connect for ~p ms...",[TS-NowTS]), State};
                        _ ->
                            ?BLstore:delete_t(Key),
                            make_schema_3({Nodes}, State)
                    end;
                _ when Mode=='all_nodes' ->
                    {retry_after, ?TIMEOUT_WAITNODES, "Expects other nodes to connect (c)", State}
            end end.
%%
make_schema_3({Nodes}, State) ->
    case lists:all(fun ping_result/1, ping(Nodes, [])) of
        false -> {retry_after, ?TIMEOUT_WAITNODES, "Expects other nodes to ping-pong", State};
        true -> try_create_schema(Nodes, State)
    end.

%% ====================================================================
%% Internal private routines
%% ====================================================================

%% ------------------------------------
%% @private
%% Pings required nodes
%% ------------------------------------
ping([], Result) -> lists:reverse(Result);
ping([Node|Rest], Result) ->
    PingResult = ?BLping:ping(Node, {1000,1000}, #{disconnect => true}),
    ping(Rest, [{Node, PingResult} | Result]).

ping_result({_, pong}) -> true;
ping_result({_, _}) -> false.

ping_mapresult({Node, pong}) -> {true, Node};
ping_mapresult({_, _}) -> false.

%% ------------------------------------
%% @private
%% Attempts to request other nodes for schema expand
%% ------------------------------------
try_request_schema([], _State) ->
    ?OUT('$force', " ~s. Mnesia try_request_schema from ~n\t~10p", [?RoleName, []]),
    ?OUT('$force', " ~s. Mnesia try_request_schema fin none", [?RoleName]),
    false;
try_request_schema(OtherNodes, State) ->
    ?OUT('$force', " ~s. Mnesia try_request_schema from ~n\t~10p", [?RoleName, OtherNodes]),
    ?BootUtils:application_start(mnesia),
    try_request_schema_1(OtherNodes, State).

%% @private
try_request_schema_1([], _State) ->
    ?BootUtils:application_stop(mnesia),
    ?OUT('$force', " ~s. Mnesia try_request_schema fin none", [?RoleName]),
    false;
try_request_schema_1([Node|Rest], State) ->
    ?OUT('$force', " ~s. Mnesia rpc:call try_request_schema from '~s'", [?RoleName, Node]),
    % 14.10.2016 rpc call directly to current module, excepting boot_srv, as it was ealier,
    %  because boot_srv could be waiting in distributed role app start fun for current node to run role app as master - it's deadlock.
    case ?BLrpc:call(Node, ?BootSchemaExpandRemote, request_expand_schema, [{?EXPANDSCHEMA_KEY, node()}], 60000) of
        {badrpc, _} ->
            try_request_schema_1(Rest, State);
        retry_after ->
            retry_after;
        false ->
            try_request_schema_1(Rest, State);
        true ->
            expand_copy_tables(Node),
            ?OUT('$force', " ~s. Mnesia try_request_schema fin '~s'", [?RoleName, Node]),
            {ok, Node}
    end.

%% ------------------------------------
%% @private
%% Creates schema on connected nodes if master,
%%   otherwise wait for master node.
%% ------------------------------------
try_create_schema([Master|_Rest]=Nodes, State) ->
    ?OUT('$force', " ~s. Mnesia try_create_schema", [?RoleName]),
    case Master == node() of
        false -> {retry_after, ?TIMEOUT_WAITMASTER, "Wait pseudo-master node " ++ ?BU:to_list(Master) ++ " to create mnesia schema", State};
        true -> ?BootSchemaCreate:create_schema(Nodes, State)
    end.

%% -------------------------------------
%% copy tables from other node
%% -------------------------------------
expand_copy_tables(_FromNode) ->
    ?OUT('$info', " ~s. Mnesia tables: ~n   ~10p", [?RoleName, lists:usort(mnesia:system_info(tables))]),
    ?MU:copy_tables().