%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Logger app general supervisor
%%% -------------------------------------------------------------------

-module(mnesialib_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1]).
-export([start_child/1, restart_child/1, terminate_child/1, delete_child/1, get_childspec/1]).
-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Args) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, Args).

%% -----------------------------------------

start_child(ChildSpec) ->
    supervisor:start_child(?MODULE, ChildSpec).

restart_child(Id) ->
    supervisor:restart_child(?MODULE, Id).

terminate_child(Id) ->
    supervisor:terminate_child(?MODULE, Id).

delete_child(Id) ->
    supervisor:delete_child(?MODULE, Id).

get_childspec(Id) ->
    supervisor:get_childspec(?MODULE, Id).

%% ====================================================================
%% Callbacks
%% ====================================================================

init(_Args) ->
    ChildSpec = [{?WORKERSUPV, {?WORKERSUPV, start_link, [[]]}, permanent, 5000, supervisor, [?WORKERSUPV]},
                 {?BootSrv, {?BootSrv, start_link, [[]]}, permanent, 5000, worker, [?BootSrv]}
                 %{?RepairSrv, {?RepairSrv, start_link, [[]]}, permanent, 1000, worker, [?RepairSrv]}
                ],
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.
