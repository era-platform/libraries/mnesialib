%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.02.2021
%%% @doc Delete & clear table routines

-module(mnesialib_table_delete).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([clear_table/1,
         delete_table/1,
         drop_table_if_appversion_changed/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

clear_table(TabName) ->
    do_clear_table(TabName).

delete_table(TabName) ->
    do_delete_table(TabName).

%% ------------------------------------------
%% attempts to delete tables if it should be changed by current application code
%%     check if owner property is changed, if not - then delete is skipped (this should remedy distributed apps)
%% ------------------------------------------
drop_table_if_appversion_changed(TabName,RecName,RecFields,Opts) ->
    case ?TableProps:is_app_version_changed(TabName,RecName,RecFields,Opts) of
        true ->
            case catch delete_table(TabName) of
                ok -> ok;
                {retry_after,_,_}=Retry -> Retry;
                {'EXIT',_} -> {retry_after,?TIMEOUT_Retry,"Exception was caught"}
            end;
        false -> false
    end.

%% ====================================================================
%% Public functions
%% ====================================================================

%% @private
%%do_delete_table(Tab) ->
%%    case ?MU:is_master_node(undefined) of
%%        false -> {retry_after, ?TIMEOUT_WaitMaster, ?BU:str("Wait for schema's master node to update table '~s'", [Tab])};
%%        true -> do_delete_table_1(Tab)
%%    end.

%% @private
do_delete_table(Tab) ->
    case mnesia:delete_table(Tab) of
        {atomic, ok} -> ok;
        {aborted, {no_exists,_}} -> ok;
        {aborted, {not_active,_,_,Nodes}} ->
            {retry_after, ?TIMEOUT_WaitReplica, ?BU:str("Cannot delete table '~s': mnesia is down on nodes ~1000p", [Tab,Nodes])};
        {aborted, Reason} ->
            {retry_after, ?TIMEOUT_WaitReplica, ?BU:str("Cannot delete table '~s': ~1000p", [Tab,Reason])}
    end.

%% ------------------------------------

%%%% @private
%%do_clear_table(Tab) ->
%%    case ?MU:is_master_node(undefined) of
%%        false -> {retry_after, ?TIMEOUT_WaitMaster, ?BU:str("Wait for schema's master node to update table '~s'",[Tab])};
%%        true -> do_clear_table_1(Tab)
%%    end.

%% @private
do_clear_table(Tab) ->
    case mnesia:wait_for_tables([Tab], 0) of
        ok -> ok;
        _ -> ?TableLoad:wait_tables_load("auto", 3, [Tab], 5000)
    end,
    case mnesia:clear_table(Tab) of
        {atomic, ok} -> ok;
        {aborted, {no_exists,_}} -> ok;
        {aborted, {not_active,_,_,Nodes}} ->
            {retry_after, ?TIMEOUT_WaitReplica, ?BU:str("Cannot clear table '~s': mnesia is down on nodes ~1000p", [Tab,Nodes])};
        {aborted, Reason} ->
            {retry_after, ?TIMEOUT_WaitReplica, ?BU:str("Cannot clear table '~s': ~1000p", [Tab,Reason])}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================