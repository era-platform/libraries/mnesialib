%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.02.2021
%%% @doc Load table routines.
%%%      Wait for load and force load. Use Timeout and Attempts.

-module(mnesialib_table_load).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([load_table/2,
         load_tables/2]).

-export([wait_tables_load/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------------------------
load_table(TabName,Opts) when is_atom(TabName), is_map(Opts) ->
    Attempts = maps:get(load_attempt_count,Opts,3),
    Timeout = maps:get(load_timeout,Opts,10000),
    do_load_table(TabName,Attempts,Timeout).

%% @private
do_load_table(TabName,Attempts,Timeout) when is_atom(TabName), is_integer(Attempts), is_integer(Timeout) ->
    wait_tables_load(?RoleName,Attempts,[TabName],Timeout).

%% ---------------------------------------
load_tables([TabName|_]=TabNames,Opts) when is_atom(TabName), is_map(Opts) ->
    Attempts = maps:get(load_attempt_count,Opts,3),
    Timeout = maps:get(load_timeout,Opts,10000),
    do_load_tables(TabNames,Attempts,Timeout).

%% @private
do_load_tables(TabNames,Attempts,Timeout) when is_list(TabNames), is_integer(Attempts), is_integer(Timeout) ->
    wait_tables_load(?RoleName,Attempts,TabNames,Timeout).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------
-spec wait_tables_load(RoleName::string(), AttemptRemain::integer(), Tables::[Name::atom()], Timeout::integer()) ->
    true | {retry_after,Timeout::integer(),Reason::string()}.
%% ----------------------------
wait_tables_load(_, _, [], _) -> true;
wait_tables_load(_, 0, [Table|_Rest], _) ->
    {retry_after, ?TIMEOUT_WaitMaster, ?BU:str("Timeout accessing table '~s', wait for replication", [Table])};
wait_tables_load(RoleName, Attempts, Tables, Timeout) ->
    case catch mnesia:wait_for_tables(Tables, Timeout) of
        ok ->
            ?OUT('$trace', "  ~s: load ~p tables ok", [RoleName,length(Tables)]),
            true;
        {timeout, TimeoutTables} ->
            ?OUT('$warning', "  ~s: timeout loading ~p tables", [RoleName,length(TimeoutTables)]),
            case force_load_tables(RoleName, TimeoutTables) of
                true -> wait_tables_load(RoleName, Attempts-1, TimeoutTables, 5000);
                Other -> Other
            end;
        _ ->
            ?OUT('$error', "  ~s: error loading tables", [RoleName]),
            case force_load_tables(RoleName, Tables) of
                true -> wait_tables_load(RoleName, Attempts-1, Tables, 5000);
                Other -> Other
            end
    end.

%% ----------------------------
-spec force_load_tables(RoleName::string(), Tables::[Name::atom()]) ->
    true | {retry_after,Timeout::integer(),Reason::list()}.
%% ----------------------------
force_load_tables(_, []) -> true;
force_load_tables(RoleName, [Table|Rest]) ->
    case catch mnesia:force_load_table(Table) of
        yes ->
            ?OUT('$info', "  ~s: table '~s' forcely loaded", [RoleName,Table]),
            force_load_tables(RoleName, Rest);
        _ErrDescr ->
            ?OUT('$error', "  ~s: table '~s' force load error: ~120p", [RoleName, Table, _ErrDescr]),
            {retry_after, ?TIMEOUT_WaitMaster, ?BU:str("Timeout accessing table '~s', wait for replication", [Table])}
    end.