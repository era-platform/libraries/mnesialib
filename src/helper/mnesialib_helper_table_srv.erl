%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.02.2021
%%% @doc Template gen_srv to attach to user application's supervisor and wait for mnesia to build/load/sync table.
%%%   After table is ready gen_srv pushes selected child to the same supervisor instead of itself.
%%%   So if you need turn your app from single mode over ets/dets to distributed mode over mnesia
%%%      then you have to attach this gen_server to your internal supervisor instead your gen_server that provide data service.

-module(mnesialib_helper_table_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-record(state, {
    ref :: reference(),
    opts
}).

%% ====================================================================
%% Public functions
%% ====================================================================

start_link(Opts) ->
    case ?BU:get_by_key('regname',Opts,undefined) of
        undefined -> gen_server:start_link(?MODULE, Opts, []);
        RegName when is_atom(RegName) -> gen_server:start_link({local,RegName}, ?MODULE, Opts, [])
    end.

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    State = #state{opts = Opts,
        ref = make_ref()},
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% restarts schema synchronization
handle_cast({callback,Ref}, #state{ref=Ref}=State) ->
    {noreply, State};

%% other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================