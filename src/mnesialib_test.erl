%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 08.02.2021
%%% @doc

-module(mnesialib_test).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([unsplit_select_rec2/2]).

-compile(export_all).

%% ====================================================================
%% Define
%% ====================================================================

%% ====================================================================
%% Public functions
%% ====================================================================

unsplit_select_rec2({_,_,Ax}=A,{_,_,Bx}=_B) when Ax > Bx -> io:format("... unsplit (A=~120p, B=~120p) -> A~n",[A,_B]), A;
unsplit_select_rec2(_A,B) -> io:format("... unsplit (A=~120p, B=~120p) -> B~n",[_A,B]), B.

create_table() ->
    mnesialib:ensure_table(tab2, rec2, [a,b], #{unsplit_method => select, unsplit_select_mfa => {mnesialib_test,unsplit_select_rec2,[]}}).
create_table3() ->
    mnesialib:ensure_table(tab3, rec2, [a,b], #{unsplit_method => select, unsplit_select_mfa => {mnesialib_test,unsplit_select_rec2,[]}}).

delete_table() ->
    mnesialib:drop_table(tab2).
delete_table3() ->
    mnesialib:drop_table(tab3).

active_replicas() ->
    mnesia:table_info(tab2,active_replicas).

put_t1() ->
    mnesia:dirty_write(tab3, {rec2,1,1}),
    mnesia:dirty_write(tab3, {rec2,2,2}),
    read_t3().

put_a1() ->
    mnesia:dirty_write(tab2, {rec2,1,1}),
    mnesia:dirty_write(tab2, {rec2,2,2}),
    read().

put_a2() ->
    mnesia:dirty_write(tab2, {rec2,3,3}),
    mnesia:dirty_write(tab2, {rec2,5,6}),
    mnesia:dirty_delete(tab2, 2),
    read().

put_a3() ->
    mnesia:dirty_write(tab2, {rec2,8,8}),
    read().

put_b2() ->
    mnesia:dirty_write(tab2, {rec2,4,4}),
    mnesia:dirty_write(tab2, {rec2,5,7}),
    mnesia:dirty_delete(tab2, 1),
    read().

read() ->
    lists:keysort(2,ets:tab2list(tab2)).
read_t3() ->
    lists:keysort(2,ets:tab2list(tab3)).

%% ====================================================================
%% Internal functions
%% ====================================================================